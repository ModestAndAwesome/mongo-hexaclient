package maa.hexaclient.mongo.collection;

import com.mongodb.client.MongoCollection;
import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;
import maa.hexaclient.mongo.operation.FindOperationBuilder;
import maa.hexaclient.mongo.operation.InsertOperation;
import maa.hexaclient.mongo.operation.OperationContext;
import org.bson.BsonDocument;

public class ObjectCollection<OBJ> {

    private final MongoCollection<BsonDocument> mongoCollection;
    private final ObjectBsonConverter<OBJ> objectConverter;
    private final ConverterRegistry registry;

    private ObjectCollection(MongoCollection<BsonDocument> mongoCollection,
                             ObjectBsonConverter<OBJ> objectConverter,
                             ConverterRegistry registry) {

        this.mongoCollection = mongoCollection;
        this.objectConverter = objectConverter;
        this.registry = registry;
    }

    public static <OBJ> ObjectCollection<OBJ> create(Class<OBJ> objectClass,
                                                     MongoCollection<BsonDocument> mongoCollection,
                                                     ConverterRegistry registry) {

        //noinspection unchecked
        final ObjectBsonConverter<OBJ> objectConverter = registry.getConverterFor(objectClass)
                .filter(ObjectBsonConverter.class::isInstance)
                .map(ObjectBsonConverter.class::cast)
                .orElseThrow(() -> new IllegalStateException("No ObjectBsonConverter found for %s".formatted(objectClass)));

        return new ObjectCollection<>(mongoCollection, objectConverter, registry);
    }

    public InsertOperation<OBJ> insert(OBJ object) {
        return new InsertOperation<>(mongoCollection, objectConverter, object);
    }

    public FindOperationBuilder<OBJ> find() {
        return new FindOperationBuilder<>(new OperationContext<>(mongoCollection, objectConverter, registry));
    }
}