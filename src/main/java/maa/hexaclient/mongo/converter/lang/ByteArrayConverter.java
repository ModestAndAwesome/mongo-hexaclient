package maa.hexaclient.mongo.converter.lang;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonBinary;
import org.bson.BsonValue;

public final class ByteArrayConverter implements BsonConverter<byte[]> {

    @Override
    public BsonValue toBson(byte[] object, ConverterContext context) {
        return new BsonBinary(object);
    }

    @Override
    public byte[] toObject(BsonValue bson, ConverterContext context) {
        return bson.asBinary().getData();
    }

    @Override
    public Class<byte[]> convertedClass() {
        return byte[].class;
    }
}
