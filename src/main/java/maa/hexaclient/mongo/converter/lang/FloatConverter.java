package maa.hexaclient.mongo.converter.lang;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonDouble;
import org.bson.BsonValue;

public class FloatConverter implements BsonConverter<Float> {

    @Override
    public BsonValue toBson(Float object, ConverterContext context) {
        return new BsonDouble(object);
    }

    @Override
    public Float toObject(BsonValue bson, ConverterContext context) {
        return (float) bson.asDouble().getValue();
    }

    @Override
    public Class<Float> convertedClass() {
        return Float.class;
    }
}
