package maa.hexaclient.mongo.converter.lang;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonBoolean;
import org.bson.BsonValue;

public final class BooleanConverter implements BsonConverter<Boolean> {

    @Override
    public BsonValue toBson(Boolean object, ConverterContext context) {
        return new BsonBoolean(object);
    }

    @Override
    public Boolean toObject(BsonValue bson, ConverterContext context) {
        return bson.asBoolean().getValue();
    }

    @Override
    public Class<Boolean> convertedClass() {
        return Boolean.class;
    }
}
