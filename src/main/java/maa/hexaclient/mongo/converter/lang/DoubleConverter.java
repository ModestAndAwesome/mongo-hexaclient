package maa.hexaclient.mongo.converter.lang;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonDouble;
import org.bson.BsonValue;

public class DoubleConverter implements BsonConverter<Double> {

    @Override
    public BsonValue toBson(Double object, ConverterContext context) {
        return new BsonDouble(object);
    }

    @Override
    public Double toObject(BsonValue bson, ConverterContext context) {
        return bson.asDouble().doubleValue();
    }

    @Override
    public Class<Double> convertedClass() {
        return Double.class;
    }
}
