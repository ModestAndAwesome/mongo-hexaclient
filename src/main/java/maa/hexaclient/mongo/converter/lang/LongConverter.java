package maa.hexaclient.mongo.converter.lang;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonInt64;
import org.bson.BsonValue;

public final class LongConverter implements BsonConverter<Long> {

    @Override
    public BsonValue toBson(Long object, ConverterContext context) {
        return new BsonInt64(object);
    }

    @Override
    public Long toObject(BsonValue bson, ConverterContext context) {
        return bson.asInt64().longValue();
    }

    @Override
    public Class<Long> convertedClass() {
        return Long.class;
    }
}
