package maa.hexaclient.mongo.converter.lang;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonInt32;
import org.bson.BsonValue;

public final class IntegerConverter implements BsonConverter<Integer> {

    @Override
    public BsonValue toBson(Integer object, ConverterContext context) {
        return new BsonInt32(object);
    }

    @Override
    public Integer toObject(BsonValue bson, ConverterContext context) {
        return bson.asInt32().intValue();
    }

    @Override
    public Class<Integer> convertedClass() {
        return Integer.class;
    }
}
