package maa.hexaclient.mongo.converter.lang;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonString;
import org.bson.BsonValue;

public final class StringConverter implements BsonConverter<String> {

    @Override
    public BsonValue toBson(String object, ConverterContext context) {
        return new BsonString(object);
    }

    @Override
    public String toObject(BsonValue bson, ConverterContext context) {
        return bson.asString().getValue();
    }

    @Override
    public Class<String> convertedClass() {
        return String.class;
    }
}
