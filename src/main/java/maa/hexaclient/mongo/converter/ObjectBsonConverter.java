package maa.hexaclient.mongo.converter;

import org.bson.BsonDocument;
import org.bson.BsonValue;

public interface ObjectBsonConverter<OBJ> extends BsonConverter<OBJ> {

    @Override
    BsonDocument toBson(OBJ object, ConverterContext context);

    @Override
    default OBJ toObject(BsonValue bson, ConverterContext context) {
        return toObject(bson.asDocument(), context);
    }

    OBJ toObject(BsonDocument bsonDocument, ConverterContext context);
}
