package maa.hexaclient.mongo.converter;

import maa.hexaclient.mongo.converter.dispatch.DispatchBsonConverter;
import maa.hexaclient.mongo.converter.dispatch.DispatchBsonConverterFactory;
import maa.hexaclient.mongo.converter.immutable.factory.ImmutableBsonConverterFactory0;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;
import maa.hexaclient.mongo.converter.wrapper.WrapperBsonConverter;
import maa.hexaclient.mongo.converter.wrapper.WrapperBsonConverterFactory;

/**
 * A factory for a {@link BsonConverter}. Allow the lazy initialisation of converters, and the use of the other converter
 * of the registry.
 *
 * @param <OBJ> The type of the object to convert
 */
public interface BsonConverterFactory<OBJ> {

    /**
     * Create a {@link BsonConverterFactory} for an immutable object. Be aware that immutable converter does not handle
     * sub classing currently. To store multiple different implementation as the same BsonDocument, us {@link #dispatch(Class)}
     *
     * @param targetClass the class which will be handled by the {@link BsonConverter}
     */
    static <O> ImmutableBsonConverterFactory0.Layer<O> forImmutable(Class<O> targetClass) {
        return new ImmutableBsonConverterFactory0.Layer<>(targetClass);
    }

    /**
     * Create a {@link BsonConverterFactory} for a {@link WrapperBsonConverter}. The wrapper converter is used to
     * flatten one sub element of a given object. Useful to store a 1 attribute value object.
     *
     * @param targetClass the class which will be handled by the {@link BsonConverter}
     */
    static <O> WrapperBsonConverterFactory.Builder0<O> forWrapper(Class<O> targetClass) {
        return new WrapperBsonConverterFactory.Builder0<>(targetClass);
    }

    /**
     * Create a {@link BsonConverterFactory} for a {@link DispatchBsonConverter}. The dispatch converter is used to handle
     * subclasses by defining a mapping between the underlying subclass, an alias, and a corresponding converter
     *
     * @param targetClass the class which will be handled by the {@link BsonConverter}
     */
    static <O> DispatchBsonConverterFactory.Builder0<O> dispatch(Class<O> targetClass) {
        return new DispatchBsonConverterFactory.Builder0<>(targetClass);
    }

    /**
     * Create a new {@link BsonConverter} using the registry provided.
     *
     * @param registry The {@link ConverterRegistry} used to find other {@link BsonConverter} to use by the created
     *                 converter
     *
     * @return a newly created converter.
     */
    BsonConverter<OBJ> create(ConverterRegistry registry);

    Class<OBJ> objectClass();
}
