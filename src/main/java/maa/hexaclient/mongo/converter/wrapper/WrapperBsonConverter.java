package maa.hexaclient.mongo.converter.wrapper;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.TwoWayConverter;
import org.bson.BsonValue;

import static java.util.Objects.requireNonNull;

/**
 * A wrapper converter will "flatten" the representation of the object by using the provided {@link TwoWayConverter} to
 * obtain the object to convert itself as the value associated with the origin object.
 *
 * @param <OBJ> The type of the object to convert
 * @param <TGT> The type of the object use in-place of our object
 */
public class WrapperBsonConverter<OBJ, TGT> implements BsonConverter<OBJ> {

    private final Class<OBJ> convertedClass;
    private final TwoWayConverter<OBJ, TGT> converter;
    private final BsonConverter<TGT> delegate;

    public WrapperBsonConverter(Class<OBJ> convertedClass, TwoWayConverter<OBJ, TGT> converter, BsonConverter<TGT> delegate) {
        this.convertedClass = requireNonNull(convertedClass);
        this.converter = requireNonNull(converter);
        this.delegate = requireNonNull(delegate);
    }

    @Override
    public BsonValue toBson(OBJ object, ConverterContext context) {
        return delegate.toBson(converter.convert1to2(object), context);
    }

    @Override
    public OBJ toObject(BsonValue bson, ConverterContext context) {
        return converter.convert2to1(delegate.toObject(bson, context));
    }

    @Override
    public Class<OBJ> convertedClass() {
        return convertedClass;
    }
}
