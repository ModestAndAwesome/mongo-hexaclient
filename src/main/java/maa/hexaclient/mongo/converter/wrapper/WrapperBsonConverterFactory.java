package maa.hexaclient.mongo.converter.wrapper;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.BsonConverterFactory;
import maa.hexaclient.mongo.converter.TwoWayConverter;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;

import java.util.function.Function;

import static java.util.Objects.requireNonNull;

/**
 * Implementation of {@link BsonConverterFactory} that build a {@link WrapperBsonConverter} from provided configuration.
 *
 * @see WrapperBsonConverter
 * @param <OBJ> The type of the object to convert
 * @param <TGT> The type of the object use in-place of our object
 */
public class WrapperBsonConverterFactory<OBJ, TGT> implements BsonConverterFactory<OBJ> {

    private final Class<OBJ> objectClass;
    private final TwoWayConverter<OBJ, TGT> converter;
    private final Class<TGT> delegateClass;

    public WrapperBsonConverterFactory(Class<OBJ> objectClass, TwoWayConverter<OBJ, TGT> converter, Class<TGT> delegateClass) {
        this.objectClass = requireNonNull(objectClass);
        this.converter = requireNonNull(converter);
        this.delegateClass = requireNonNull(delegateClass);
    }

    @Override
    public BsonConverter<OBJ> create(ConverterRegistry registry) {
        return new WrapperBsonConverter<>(objectClass,
                converter,
                registry.getMandatoryConverterFor(delegateClass)
        );
    }

    @Override
    public Class<OBJ> objectClass() {
        return objectClass;
    }

    public static class Builder0<OBJ> {

        private final Class<OBJ> encoderClass;

        public Builder0(Class<OBJ> encoderClass) {
            this.encoderClass = encoderClass;
        }

        public <TGT> WrapperBsonConverterFactory.Builder1<OBJ, TGT> delegate(Function<OBJ, TGT> encodingMethod, Function<TGT, OBJ> decodingMethod) {
            return new Builder1<>(encoderClass, TwoWayConverter.fromFunctions(encodingMethod, decodingMethod));
        }

        public <TGT> WrapperBsonConverterFactory.Builder1<OBJ, TGT> delegate(TwoWayConverter<OBJ, TGT> converter) {
            return new Builder1<>(encoderClass, converter);
        }
    }

    public static class Builder1<OBJ, TGT> {

        private final Class<OBJ> encoderClass;
        private final TwoWayConverter<OBJ, TGT> converter;

        public Builder1(Class<OBJ> encoderClass, TwoWayConverter<OBJ, TGT> converter) {
            this.encoderClass = requireNonNull(encoderClass);
            this.converter = requireNonNull(converter);
        }

        public WrapperBsonConverterFactory<OBJ, TGT> as(Class<TGT> delegateClass) {
            return new WrapperBsonConverterFactory<>(encoderClass, converter, delegateClass);
        }
    }
}
