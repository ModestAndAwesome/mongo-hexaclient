package maa.hexaclient.mongo.converter.time;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonDateTime;
import org.bson.BsonValue;

import java.time.Instant;

public final class InstantConverter implements BsonConverter<Instant> {

    @Override
    public BsonValue toBson(Instant object, ConverterContext context) {
        return new BsonDateTime(object.toEpochMilli());
    }

    @Override
    public Instant toObject(BsonValue bson, ConverterContext context) {
        return Instant.ofEpochMilli(bson.asDateTime().getValue());
    }

    @Override
    public Class<Instant> convertedClass() {
        return Instant.class;
    }
}
