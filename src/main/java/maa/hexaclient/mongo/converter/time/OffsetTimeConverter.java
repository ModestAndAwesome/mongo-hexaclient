package maa.hexaclient.mongo.converter.time;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonDateTime;
import org.bson.BsonValue;

import java.time.Instant;
import java.time.LocalDate;
import java.time.OffsetTime;
import java.time.ZoneId;

public class OffsetTimeConverter implements BsonConverter<OffsetTime> {

    private final ZoneId zoneId;

    public OffsetTimeConverter(ZoneId zoneId) {
        this.zoneId = zoneId;
    }

    @Override
    public BsonValue toBson(OffsetTime object, ConverterContext context) {
        return new BsonDateTime(object.atDate(LocalDate.EPOCH).atZoneSameInstant(zoneId).toInstant().toEpochMilli());
    }

    @Override
    public OffsetTime toObject(BsonValue bson, ConverterContext context) {
        return Instant.ofEpochMilli(bson.asDateTime().getValue()).atZone(zoneId).toOffsetDateTime().toOffsetTime();
    }

    @Override
    public Class<OffsetTime> convertedClass() {
        return OffsetTime.class;
    }
}
