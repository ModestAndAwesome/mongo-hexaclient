package maa.hexaclient.mongo.converter.time;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonDateTime;
import org.bson.BsonValue;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public final class ZonedDateTimeConverter implements BsonConverter<ZonedDateTime> {

    private final ZoneId zoneId;

    public ZonedDateTimeConverter(ZoneId zoneId) {
        this.zoneId = zoneId;
    }

    @Override
    public BsonValue toBson(ZonedDateTime object, ConverterContext context) {
        return new BsonDateTime(object.toInstant().toEpochMilli());
    }

    @Override
    public ZonedDateTime toObject(BsonValue bson, ConverterContext context) {
        return Instant.ofEpochMilli(bson.asDateTime().getValue()).atZone(zoneId);
    }

    @Override
    public Class<ZonedDateTime> convertedClass() {
        return ZonedDateTime.class;
    }
}
