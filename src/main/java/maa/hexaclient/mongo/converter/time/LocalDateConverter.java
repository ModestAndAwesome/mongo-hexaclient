package maa.hexaclient.mongo.converter.time;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonDateTime;
import org.bson.BsonValue;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

public final class LocalDateConverter implements BsonConverter<LocalDate> {

    private final ZoneId zoneId;

    public LocalDateConverter(ZoneId zoneId) {
        this.zoneId = zoneId;
    }

    @Override
    public BsonValue toBson(LocalDate object, ConverterContext context) {
        return new BsonDateTime(object.atStartOfDay().atZone(zoneId).toInstant().toEpochMilli());
    }

    @Override
    public LocalDate toObject(BsonValue bson, ConverterContext context) {
        return Instant.ofEpochMilli(bson.asDateTime().getValue()).atZone(zoneId).toLocalDate();
    }

    @Override
    public Class<LocalDate> convertedClass() {
        return LocalDate.class;
    }
}
