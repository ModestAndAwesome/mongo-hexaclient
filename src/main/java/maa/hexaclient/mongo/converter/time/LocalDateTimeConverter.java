package maa.hexaclient.mongo.converter.time;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonDateTime;
import org.bson.BsonValue;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public final class LocalDateTimeConverter implements BsonConverter<LocalDateTime> {

    private final ZoneId zoneId;

    public LocalDateTimeConverter(ZoneId zoneId) {
        this.zoneId = zoneId;
    }

    @Override
    public BsonValue toBson(LocalDateTime object, ConverterContext context) {
        return new BsonDateTime(object.atZone(zoneId).toInstant().toEpochMilli());
    }

    @Override
    public LocalDateTime toObject(BsonValue bson, ConverterContext context) {
        return Instant.ofEpochMilli(bson.asDateTime().getValue()).atZone(zoneId).toLocalDateTime();
    }

    @Override
    public Class<LocalDateTime> convertedClass() {
        return LocalDateTime.class;
    }
}
