package maa.hexaclient.mongo.converter.time;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonDateTime;
import org.bson.BsonValue;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Objects;

public class LocalTimeConverter implements BsonConverter<LocalTime> {

    private final ZoneId zoneId;

    public LocalTimeConverter(ZoneId zoneId) {
        this.zoneId = Objects.requireNonNull(zoneId);
    }

    @Override
    public BsonValue toBson(LocalTime object, ConverterContext context) {
        return new BsonDateTime(object.atDate(LocalDate.EPOCH).atZone(zoneId).toInstant().toEpochMilli());
    }

    @Override
    public LocalTime toObject(BsonValue bson, ConverterContext context) {
        return Instant.ofEpochMilli(bson.asDateTime().getValue()).atZone(zoneId).toLocalTime();
    }

    @Override
    public Class<LocalTime> convertedClass() {
        return LocalTime.class;
    }
}
