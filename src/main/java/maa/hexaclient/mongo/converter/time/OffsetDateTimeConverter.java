package maa.hexaclient.mongo.converter.time;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonDateTime;
import org.bson.BsonValue;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;

public class OffsetDateTimeConverter implements BsonConverter<OffsetDateTime> {

    private final ZoneId zoneId;

    public OffsetDateTimeConverter(ZoneId zoneId) {
        this.zoneId = zoneId;
    }

    @Override
    public BsonValue toBson(OffsetDateTime object, ConverterContext context) {
        return new BsonDateTime(object.atZoneSameInstant(zoneId).toInstant().toEpochMilli());
    }

    @Override
    public OffsetDateTime toObject(BsonValue bson, ConverterContext context) {
        return OffsetDateTime.ofInstant(Instant.ofEpochMilli(bson.asDateTime().getValue()), zoneId);
    }

    @Override
    public Class<OffsetDateTime> convertedClass() {
        return OffsetDateTime.class;
    }
}
