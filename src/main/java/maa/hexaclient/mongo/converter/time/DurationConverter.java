package maa.hexaclient.mongo.converter.time;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonInt64;
import org.bson.BsonValue;

import java.time.Duration;

public class DurationConverter implements BsonConverter<Duration> {

    @Override
    public BsonValue toBson(Duration object, ConverterContext context) {
        return new BsonInt64(object.toMillis());
    }

    @Override
    public Duration toObject(BsonValue bson, ConverterContext context) {
        return Duration.ofMillis(bson.asInt64().getValue());
    }

    @Override
    public Class<Duration> convertedClass() {
        return Duration.class;
    }
}
