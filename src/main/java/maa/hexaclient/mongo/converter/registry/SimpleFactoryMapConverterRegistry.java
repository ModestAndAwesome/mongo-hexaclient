package maa.hexaclient.mongo.converter.registry;

import maa.hexaclient.mongo.converter.BsonConverterFactory;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class SimpleFactoryMapConverterRegistry extends FactoryMapConverterRegistry {

    public SimpleFactoryMapConverterRegistry(Map<Class<?>, BsonConverterFactory<?>> factories) {
        super(factories);
    }

    public static FactoryMapConverterRegistry of(BsonConverterFactory<?>... factories) {
        return new SimpleFactoryMapConverterRegistry(Arrays.stream(factories)
                .collect(Collectors.toMap(BsonConverterFactory::objectClass, Function.identity())));
    }
}
