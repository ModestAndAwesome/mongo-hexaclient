package maa.hexaclient.mongo.converter.registry;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.BsonConverterFactory;
import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.lang.BooleanConverter;
import maa.hexaclient.mongo.converter.lang.ByteArrayConverter;
import maa.hexaclient.mongo.converter.lang.DoubleConverter;
import maa.hexaclient.mongo.converter.lang.FloatConverter;
import maa.hexaclient.mongo.converter.lang.IntegerConverter;
import maa.hexaclient.mongo.converter.lang.LongConverter;
import maa.hexaclient.mongo.converter.lang.StringConverter;
import maa.hexaclient.mongo.converter.time.DurationConverter;
import maa.hexaclient.mongo.converter.time.InstantConverter;
import maa.hexaclient.mongo.converter.time.LocalDateConverter;
import maa.hexaclient.mongo.converter.time.LocalDateTimeConverter;
import maa.hexaclient.mongo.converter.time.LocalTimeConverter;
import maa.hexaclient.mongo.converter.time.OffsetDateTimeConverter;
import maa.hexaclient.mongo.converter.time.OffsetTimeConverter;
import maa.hexaclient.mongo.converter.time.ZonedDateTimeConverter;
import org.bson.BsonValue;

import java.time.*;
import java.util.*;

/**
 * A {@link ConverterRegistry} is an object referencing {@link BsonConverter} by there converted class. The goal is
 * to have a central point to retrieve any required converter.
 *
 */
public interface ConverterRegistry {

    <OBJ> Optional<BsonConverter<OBJ>> getConverterFor(Class<OBJ> objectClass, ConverterRegistry parent);

    default <OBJ> Optional<BsonConverter<OBJ>> getConverterFor(Class<OBJ> objectClass) {
        return getConverterFor(objectClass, this);
    }

    default <OBJ> BsonConverter<OBJ> getMandatoryConverterFor(Class<OBJ> objectClass) {
        return getConverterFor(objectClass)
                .orElseThrow(() -> new IllegalStateException("No Converter found for class %s".formatted(objectClass)));
    }

    default <OBJ> Optional<BsonValue> convert(OBJ object, ConverterContext context) {
        //noinspection unchecked
        return getConverterFor((Class<OBJ>) object.getClass()).map(converter -> converter.toBson(object, context));
    }

    default <OBJ> BsonValue mandatoryConvert(OBJ object, ConverterContext context) {
        return convert(object, context)
                .orElseThrow(() -> new IllegalStateException("No Converter found for class %s".formatted(object.getClass())));
    }

    default CachedConverterRegistry cached() {
        return new CachedConverterRegistry(this);
    }


    static ComposedConverterRegistry composedOf(ConverterRegistry... registries) {
        return ComposedConverterRegistry.of(registries);
    }

    static MapConverterRegistry of(BsonConverter<?>... converters) {
        return MapConverterRegistry.of(converters);
    }

    static FactoryMapConverterRegistry of(BsonConverterFactory<?>... factories) {
        return SimpleFactoryMapConverterRegistry.of(factories);
    }

    static MapConverterRegistry lang() {
        return new MapConverterRegistry(Map.ofEntries(
                Map.entry(Boolean.class, new BooleanConverter()),
                Map.entry(byte[].class, new ByteArrayConverter()),
                Map.entry(Instant.class, new InstantConverter()),
                Map.entry(Integer.class, new IntegerConverter()),
                Map.entry(Long.class, new LongConverter()),
                Map.entry(String.class, new StringConverter()),
                Map.entry(Float.class, new FloatConverter()),
                Map.entry(Double.class, new DoubleConverter())
        ));
    }

    static MapConverterRegistry utc() {
        return time(ZoneOffset.UTC);
    }

    static MapConverterRegistry time(ZoneId zoneId) {
        return new MapConverterRegistry(Map.ofEntries(
                Map.entry(Duration.class, new DurationConverter()),
                Map.entry(Instant.class, new InstantConverter()),
                Map.entry(LocalDate.class, new LocalDateConverter(zoneId)),
                Map.entry(LocalDateTime.class, new LocalDateTimeConverter(zoneId)),
                Map.entry(LocalTime.class, new LocalTimeConverter(zoneId)),
                Map.entry(OffsetDateTime.class, new OffsetDateTimeConverter(zoneId)),
                Map.entry(OffsetTime.class, new OffsetTimeConverter(zoneId)),
                Map.entry(ZonedDateTime.class, new ZonedDateTimeConverter(zoneId))
        ));
    }
}
