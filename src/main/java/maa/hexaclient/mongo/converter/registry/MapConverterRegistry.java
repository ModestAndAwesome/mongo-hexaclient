package maa.hexaclient.mongo.converter.registry;

import maa.hexaclient.mongo.converter.BsonConverter;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MapConverterRegistry implements ConverterRegistry {

    private final Map<Class<?>, BsonConverter<?>> converters;

    public MapConverterRegistry(Map<Class<?>, BsonConverter<?>> converters) {
        this.converters = Map.copyOf(converters);
    }

    public static MapConverterRegistry of(BsonConverter<?>... converters) {
        return new MapConverterRegistry(Arrays.stream(converters)
                .collect(Collectors.toMap(BsonConverter::convertedClass, Function.identity())));
    }

    @Override
    public <OBJ> Optional<BsonConverter<OBJ>> getConverterFor(Class<OBJ> objectClass, ConverterRegistry parent) {
        //noinspection unchecked
        return Optional.ofNullable((BsonConverter<OBJ>) converters.get(objectClass));
    }
}
