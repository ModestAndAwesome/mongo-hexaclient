package maa.hexaclient.mongo.converter.registry;

import maa.hexaclient.mongo.converter.BsonConverter;

import java.util.Optional;
import java.util.Set;

public class ComposedConverterRegistry implements ConverterRegistry {

    private final Set<ConverterRegistry> delegates;

    public ComposedConverterRegistry(Set<ConverterRegistry> delegates) {
        this.delegates = Set.copyOf(delegates);
    }

    public static ComposedConverterRegistry of(ConverterRegistry... registries) {
        return new ComposedConverterRegistry(Set.of(registries));
    }

    @Override
    public <OBJ> Optional<BsonConverter<OBJ>> getConverterFor(Class<OBJ> objectClass, ConverterRegistry parent) {
        return delegates.stream()
                .map(registry -> registry.getConverterFor(objectClass, this))
                .flatMap(Optional::stream)
                .findFirst();
    }
}
