package maa.hexaclient.mongo.converter.registry;

import maa.hexaclient.mongo.converter.BsonConverter;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class CachedConverterRegistry implements ConverterRegistry {

    private final Map<Class<?>, Optional<BsonConverter<?>>> cache = Collections.synchronizedMap(new HashMap<>());
    private final ConverterRegistry delegate;

    public CachedConverterRegistry(ConverterRegistry delegate) {
        this.delegate = delegate;
    }

    @Override
    public <OBJ> Optional<BsonConverter<OBJ>> getConverterFor(Class<OBJ> objectClass, ConverterRegistry parent) {
        //noinspection unchecked
        return cache.computeIfAbsent(objectClass, this::retrieveFromDelegate)
                .map(BsonConverter.class::cast);
    }

    private <GNI> Optional<GNI> retrieveFromDelegate(Class<?> clazz) {
        //noinspection unchecked
        return (Optional<GNI>) delegate.getConverterFor(clazz);
    }
}
