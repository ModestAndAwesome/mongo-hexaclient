package maa.hexaclient.mongo.converter.registry;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.BsonConverterFactory;

import java.util.Map;
import java.util.Optional;

public abstract class FactoryMapConverterRegistry implements ConverterRegistry {
    protected final Map<Class<?>, ? extends BsonConverterFactory<?>> factories;

    protected FactoryMapConverterRegistry(Map<Class<?>, ? extends BsonConverterFactory<?>> factories) {
        this.factories = Map.copyOf(factories);
    }

    @Override
    public <OBJ> Optional<BsonConverter<OBJ>> getConverterFor(Class<OBJ> objectClass, ConverterRegistry parent) {
        //noinspection unchecked
        return Optional.ofNullable(factories.get(objectClass))
                .map(factory -> (BsonConverter<OBJ>) factory.create(parent));
    }
}
