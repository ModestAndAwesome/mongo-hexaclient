package maa.hexaclient.mongo.converter;

import maa.hexaclient.mongo.ToBsonConverter;
import org.bson.BsonValue;

/**
 * <p>
 * A {@link BsonConverter} is an object allowing a given object ot type OBJ to be converted to a {@link BsonValue} and back
 * to an object from it.
 * </p>
 * <p>
 *     Note that there is no type parameter on the {@link BsonValue} type, but some sub interface such as {@link ObjectBsonConverter}
 *     have overridden the used type.
 * </p>
 *
 * @param <OBJ> The type of the object to be converted to a {@link BsonValue}
 */
public interface BsonConverter<OBJ> extends ToBsonConverter<OBJ> {

    OBJ toObject(BsonValue bson, ConverterContext context);

    Class<OBJ> convertedClass();
}
