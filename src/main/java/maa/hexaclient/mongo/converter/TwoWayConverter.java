package maa.hexaclient.mongo.converter;

import java.util.function.Function;

public interface TwoWayConverter<Attribute1, Attribute2> {

    Attribute2 convert1to2(Attribute1 attribute1);

    Attribute1 convert2to1(Attribute2 attribute2);

    static <Attribute1, Attribute2> TwoWayConverter<Attribute1, Attribute2> fromFunctions(
            Function<Attribute1, Attribute2> function1to2,
            Function<Attribute2, Attribute1> function2to1
    ) {
        return new FromFunctions<>(function1to2, function2to1);
    }

    record FromFunctions<Attribute1, Attribute2>(Function<Attribute1, Attribute2> function1to2, Function<Attribute2, Attribute1> function2to1)
        implements TwoWayConverter<Attribute1, Attribute2>
    {
        @Override
        public Attribute2 convert1to2(Attribute1 attribute1) {
            return function1to2.apply(attribute1);
        }

        @Override
        public Attribute1 convert2to1(Attribute2 attribute2) {
            return function2to1.apply(attribute2);
        }
    }

}
