package maa.hexaclient.mongo.converter.dispatch;

import maa.hexaclient.mongo.converter.ObjectBsonConverter;

public record Dispatch<SUB>(String typeAlias, ObjectBsonConverter<SUB> converter) {}
