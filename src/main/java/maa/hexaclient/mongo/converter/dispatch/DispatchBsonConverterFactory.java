package maa.hexaclient.mongo.converter.dispatch;

import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import maa.hexaclient.mongo.converter.ObjectBsonConverterFactory;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

public class DispatchBsonConverterFactory<OBJ> implements ObjectBsonConverterFactory<OBJ> {
    private final Class<OBJ> objectClass;
    private final String dispatchAttribute;
    private final List<DispatchFactory<? extends OBJ>> dispatchFactories;
    private final Supplier<OBJ> otherwise;

    public DispatchBsonConverterFactory(Class<OBJ> objectClass, String dispatchAttribute, List<DispatchFactory<? extends OBJ>> dispatchFactories, Supplier<OBJ> otherwise) {
        this.objectClass = requireNonNull(objectClass);
        this.dispatchAttribute = requireNonNull(dispatchAttribute);
        this.dispatchFactories = List.copyOf(requireNonNull(dispatchFactories));
        this.otherwise = otherwise;
    }

    @Override
    public ObjectBsonConverter<OBJ> create(ConverterRegistry registry) {

        final var initier = new DispatchFactory.Initier(registry);

        final Dispatcher<OBJ> dispatcher = Dispatcher.from(dispatchFactories.stream()
                .map(factory -> factory.resolve(initier))
                .collect(Collectors.toUnmodifiableSet()));

        return new DispatchBsonConverter<>(objectClass, dispatchAttribute, dispatcher, otherwise);
    }

    @Override
    public Class<OBJ> objectClass() {
        return objectClass;
    }

    public static class Builder0<OBJ> {
        private final Class<OBJ> targetClass;

        public Builder0(Class<OBJ> targetClass) {
            this.targetClass = requireNonNull(targetClass);
        }

        public Builder1<OBJ> on(String dispatcher) {
            return new Builder1<>(targetClass, dispatcher);
        }
    }

    public static class Builder1<OBJ> {
        private final Class<OBJ> targetClass;
        private final String dispatchAttribute;
        private final List<DispatchFactory<? extends OBJ>> dispatches = new ArrayList<>();

        private Builder1(Class<OBJ> targetClass, String dispatchAttribute) {
            this.targetClass = requireNonNull(targetClass);
            this.dispatchAttribute = requireNonNull(dispatchAttribute);
        }

        public <SUB extends OBJ> Builder1<OBJ> dispatch(DispatchFactory<SUB> dispatch) {
            dispatches.add(dispatch);
            return this;
        }

        public Finisher<OBJ> otherwise() {
            return new Finisher<>(this);
        }
    }

    public static class Finisher<OBJ> {
        private final Builder1<OBJ> builder1;

        private Finisher(Builder1<OBJ> builder1) {
            this.builder1 = requireNonNull(builder1);
        }

        public DispatchBsonConverterFactory<OBJ> returnNull() {
            return new DispatchBsonConverterFactory<>(builder1.targetClass, builder1.dispatchAttribute, builder1.dispatches, () -> null);
        }
    }
}
