package maa.hexaclient.mongo.converter.dispatch;

import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import maa.hexaclient.mongo.converter.ObjectBsonConverterFactory;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;

@FunctionalInterface
public interface DispatchFactory<OBJ> {

    Dispatch<OBJ> resolve(Initier initier);

    class Initier {
        private final ConverterRegistry registry;

        public Initier(ConverterRegistry registry) {
            this.registry = registry;
        }

        public Builder alias(String typeAlias) {
            return new Builder(registry, typeAlias);
        }
    }

    class Builder {
        private final ConverterRegistry registry;
        private final String typeAlias;

        public Builder(ConverterRegistry registry, String typeAlias) {
            this.registry = registry;
            this.typeAlias = typeAlias;
        }

        public <OBJ> Dispatch<OBJ> as(Class<OBJ> targetClass) {
            //noinspection unchecked
            return using(registry.getConverterFor(targetClass)
                    .filter(ObjectBsonConverter.class::isInstance)
                    .map(ObjectBsonConverter.class::cast)
                    .orElseThrow(() -> new IllegalStateException("No ObjectCodec found for %s".formatted(targetClass))));
        }

        public <OBJ> Dispatch<OBJ> using(ObjectBsonConverterFactory<OBJ> targetCodecFactory) {
            return using(targetCodecFactory.create(registry));
        }

        public <OBJ> Dispatch<OBJ> using(ObjectBsonConverter<OBJ> converter) {
            return new Dispatch<>(typeAlias, converter);
        }
    }

}
