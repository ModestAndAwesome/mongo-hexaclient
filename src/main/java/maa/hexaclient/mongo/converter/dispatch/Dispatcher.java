package maa.hexaclient.mongo.converter.dispatch;

import maa.hexaclient.mongo.converter.ObjectBsonConverter;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Dispatcher<OBJ> {

    private final Map<String, Dispatch<? extends OBJ>> fromTypeAlias;
    private final Map<Class<? extends OBJ>, Dispatch<? extends OBJ>> fromClass;

    private Dispatcher(Map<String, Dispatch<? extends OBJ>> fromTypeAlias, Map<Class<? extends OBJ>, Dispatch<? extends OBJ>> fromClass) {
        this.fromTypeAlias = fromTypeAlias;
        this.fromClass = fromClass;
    }

    public static <OBJ> Dispatcher<OBJ> from(Collection<Dispatch<? extends OBJ>> dispatches) {
        final Map<String, Dispatch<? extends OBJ>> fromTypeAlias = dispatches.stream()
                .collect(Collectors.toMap(Dispatch::typeAlias, Function.identity()));

        final Map<Class<? extends OBJ>, Dispatch<? extends OBJ>> fromClass = dispatches.stream()
                .collect(Collectors.toMap(dispatch -> dispatch.converter().convertedClass(), Function.identity()));

        return new Dispatcher<>(fromTypeAlias, fromClass);
    }

    public Optional<ObjectBsonConverter<? extends OBJ>> findDispatch(String typeAlias) {
        return Optional.ofNullable(fromTypeAlias.get(typeAlias)).map(Dispatch::converter);
    }

    public <SUB extends OBJ> Optional<Dispatch<SUB>> findDispatch(Class<SUB> subClass) {
        //noinspection unchecked
        return Optional.ofNullable(fromClass.get(subClass)).map(Dispatch.class::cast);
    }
}
