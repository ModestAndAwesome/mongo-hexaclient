package maa.hexaclient.mongo.converter.dispatch;

import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import org.bson.BsonDocument;
import org.bson.BsonString;

import java.util.Optional;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

public class DispatchBsonConverter<OBJ> implements ObjectBsonConverter<OBJ> {

    private final Class<OBJ> convertedClass;
    private final String dispatchAttribute;
    private final Dispatcher<OBJ> dispatcher;
    private final Supplier<OBJ> otherwise;

    public DispatchBsonConverter(Class<OBJ> convertedClass, String dispatchAttribute,
                                 Dispatcher<OBJ> dispatcher,
                                 Supplier<OBJ> otherwise) {

        this.convertedClass = requireNonNull(convertedClass);
        this.dispatchAttribute = requireNonNull(dispatchAttribute);
        this.dispatcher = requireNonNull(dispatcher);
        this.otherwise = requireNonNull(otherwise);
    }

    @Override
    public Class<OBJ> convertedClass() {
        return convertedClass;
    }

    @Override
    public BsonDocument toBson(OBJ object, ConverterContext context) {
        requireNonNull(object);
        requireNonNull(context);

        return toBsonInternal(object, context);
    }

    private <SUB extends OBJ> BsonDocument toBsonInternal(SUB object, ConverterContext context) {

        //noinspection unchecked
        return dispatcher.findDispatch((Class<SUB>) object.getClass())
                .map(dispatch -> dispatch.converter()
                        .toBson(object, context)
                        .append(dispatchAttribute, new BsonString(dispatch.typeAlias())))
                .orElseGet(BsonDocument::new);
    }

    @Override
    public OBJ toObject(BsonDocument bsonDocument, ConverterContext context) {

        return Optional.ofNullable(bsonDocument.getString(dispatchAttribute))
                .flatMap(typeAlias -> dispatcher.findDispatch(typeAlias.getValue()))
                .map(objectBsonConverter -> (OBJ) objectBsonConverter.toObject(bsonDocument, context))
                .orElseGet(otherwise);
    }

}
