package maa.hexaclient.mongo.converter;

public class ConversionException extends RuntimeException {

    public ConversionException(String message) {
        super(message);
    }
}
