package maa.hexaclient.mongo.converter;

import maa.hexaclient.mongo.converter.registry.ConverterRegistry;

/**
 * A {@link BsonConverterFactory} to build {@link ObjectBsonConverter}.
 *
 * {@inheritDoc}
 */
public interface ObjectBsonConverterFactory<OBJ> extends BsonConverterFactory<OBJ> {

    @Override
    ObjectBsonConverter<OBJ> create(ConverterRegistry registry);

}
