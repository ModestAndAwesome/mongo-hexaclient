package maa.hexaclient.mongo.converter.immutable;

import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import maa.hexaclient.mongo.converter.immutable.attribute.ImmutableAttribute;
import org.bson.BsonDocument;

import static java.util.Objects.requireNonNull;

public record ImmutableBsonConverter1<OBJ, ATTR1>(
        ImmutableBsonConverter1.Layer<OBJ, ATTR1> parent,
        ImmutableBsonConverter1.Constructor<OBJ, ATTR1> constructor
) implements ObjectBsonConverter<OBJ> {

    public ImmutableBsonConverter1(Layer<OBJ, ATTR1> parent, Constructor<OBJ, ATTR1> constructor) {
        this.parent = requireNonNull(parent);
        this.constructor = requireNonNull(constructor);
    }

    @Override
    public BsonDocument toBson(OBJ object, ConverterContext context) {
        requireNonNull(object);
        requireNonNull(context);

        return parent.toBson(object, context);
    }

    @Override
    public OBJ toObject(BsonDocument bsonDocument, ConverterContext context) {
        return parent.toObject(bsonDocument, context, constructor);
    }

    @Override
    public Class<OBJ> convertedClass() {
        return parent.convertedClass();
    }

    public static final class Layer<OBJ, ATTR1> {
        private final ImmutableBsonConverter0.Layer<OBJ> parent;
        private final ImmutableAttribute<OBJ, ATTR1> attribute;

        public Layer(ImmutableBsonConverter0.Layer<OBJ> parent, ImmutableAttribute<OBJ, ATTR1> attribute) {
            this.parent = requireNonNull(parent);
            this.attribute = requireNonNull(attribute);
        }

        public BsonDocument toBson(OBJ object, ConverterContext context) {
            return attribute.appendTo(parent.toBson(), object, context);
        }

        public OBJ toObject(BsonDocument bsonDocument, ConverterContext context, Constructor<OBJ, ATTR1> constructor) {
            return constructor.construct(attribute.retrieveValue(bsonDocument, context).orElse(null));
        }

        public Class<OBJ> convertedClass() {
            return parent.convertedClass();
        }

        public ImmutableBsonConverter0.Layer<OBJ> parent() {
            return parent;
        }

        public ImmutableAttribute<OBJ, ATTR1> attribute() {
            return attribute;
        }
    }

    public interface Constructor<OBJ, ATTR1> {
        OBJ construct(ATTR1 attr1);
    }
}
