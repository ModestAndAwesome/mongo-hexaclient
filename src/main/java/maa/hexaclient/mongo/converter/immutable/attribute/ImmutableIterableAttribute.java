package maa.hexaclient.mongo.converter.immutable.attribute;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonArray;
import org.bson.BsonDocument;

import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.StreamSupport;

import static java.util.Objects.requireNonNull;

@SuppressWarnings("ClassCanBeRecord")
public final class ImmutableIterableAttribute<OBJ, ATTR, TMP, IT extends Iterable<ATTR>> implements ImmutableAttribute<OBJ, IT> {

    private final String name;
    private final Function<OBJ, IT> extractor;
    private final BsonConverter<ATTR> itemConverter;
    private final Collector<ATTR, TMP, IT> collector;

    public ImmutableIterableAttribute(String name, Function<OBJ, IT> extractor, BsonConverter<ATTR> itemConverter, Collector<ATTR, TMP, IT> collector) {
        this.name = requireNonNull(name);
        this.extractor = requireNonNull(extractor);
        this.itemConverter = requireNonNull(itemConverter);
        this.collector = requireNonNull(collector);
    }

    @Override
    public BsonDocument appendTo(BsonDocument bsonDocument, OBJ object, ConverterContext context) {
        return bsonDocument.append(
                name,
                StreamSupport.stream(extractor.apply(object).spliterator(), false)
                        .map(item -> itemConverter.toBson(item, context))
                        .collect(BsonArray::new, BsonArray::add, BsonArray::addAll)
        );
    }

    @Override
    public Optional<IT> retrieveValue(BsonDocument bsonDocument, ConverterContext context) {
        if (bsonDocument.isNull()) {
            return Optional.of(collector.finisher().apply(collector.supplier().get()));
        }
        return Optional.of(bsonDocument.getArray(name)
                .stream()
                .map(bsonValue -> itemConverter.toObject(bsonValue, context))
                .collect(collector));
    }

}
