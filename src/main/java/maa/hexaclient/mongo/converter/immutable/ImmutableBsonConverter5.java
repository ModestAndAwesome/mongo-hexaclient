package maa.hexaclient.mongo.converter.immutable;

import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import maa.hexaclient.mongo.converter.immutable.attribute.ImmutableAttribute;
import org.bson.BsonDocument;

import static java.util.Objects.requireNonNull;

public record ImmutableBsonConverter5<OBJ, ATTR1, ATTR2, ATTR3, ATTR4, ATTR5>(
        ImmutableBsonConverter5.Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4, ATTR5> parent,
        ImmutableBsonConverter5.Constructor<OBJ, ATTR1, ATTR2, ATTR3, ATTR4, ATTR5> constructor
) implements ObjectBsonConverter<OBJ> {

    public ImmutableBsonConverter5 {
        requireNonNull(parent);
        requireNonNull(constructor);
    }

    @Override
    public BsonDocument toBson(OBJ object, ConverterContext context) {
        requireNonNull(object);
        requireNonNull(context);

        return parent.toBson(object, context);
    }

    @Override
    public OBJ toObject(BsonDocument bsonDocument, ConverterContext context) {
        return parent.toObject(bsonDocument, context, constructor);
    }

    @Override
    public Class<OBJ> convertedClass() {
        return parent.convertedClass();
    }

    public static final class Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4, ATTR5> {
        private final ImmutableBsonConverter4.Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> parent;
        private final ImmutableAttribute<OBJ, ATTR5> attribute;

        public Layer(ImmutableBsonConverter4.Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> parent, ImmutableAttribute<OBJ, ATTR5> attribute) {
            this.parent = requireNonNull(parent);
            this.attribute = requireNonNull(attribute);
        }

        public BsonDocument toBson(OBJ object, ConverterContext context) {
            return attribute.appendTo(parent.toBson(object, context), object, context);
        }

        public OBJ toObject(BsonDocument bsonDocument, ConverterContext context, Constructor<OBJ, ATTR1, ATTR2, ATTR3, ATTR4, ATTR5> constructor) {
            return parent.toObject(bsonDocument,
                    context,
                    (attr1, attr2, attr3, attr4) ->
                            constructor.construct(attr1, attr2, attr3, attr4, attribute.retrieveValue(bsonDocument, context).orElse(null)));
        }

        public Class<OBJ> convertedClass() {
            return parent.convertedClass();
        }

        public ImmutableBsonConverter4.Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> parent() {
            return parent;
        }

        public ImmutableAttribute<OBJ, ATTR5> attribute() {
            return attribute;
        }
    }

    public interface Constructor<OBJ, ATTR1, ATTR2, ATTR3, ATTR4, ATTR5> {
        OBJ construct(ATTR1 attr1, ATTR2 attr2, ATTR3 attr3, ATTR4 attr4, ATTR5 attr5);
    }
}
