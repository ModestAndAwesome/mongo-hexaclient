package maa.hexaclient.mongo.converter.immutable.factory;

import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import maa.hexaclient.mongo.converter.ObjectBsonConverterFactory;
import maa.hexaclient.mongo.converter.immutable.ImmutableBsonConverter5;
import maa.hexaclient.mongo.converter.immutable.attribute.ImmutableAttributeFactory;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;

import static java.util.Objects.requireNonNull;

public record ImmutableBsonConverterFactory5<OBJ, ATTR1, ATTR2, ATTR3, ATTR4, ATTR5>(
        ImmutableBsonConverterFactory5.Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4, ATTR5> parent,
        ImmutableBsonConverter5.Constructor<OBJ, ATTR1, ATTR2, ATTR3, ATTR4, ATTR5> constructor
) implements ObjectBsonConverterFactory<OBJ> {

    @Override
    public ObjectBsonConverter<OBJ> create(ConverterRegistry registry) {
        return new ImmutableBsonConverter5<>(parent.create(registry), constructor);
    }

    @Override
    public Class<OBJ> objectClass() {
        return parent.objectClass();
    }

    public abstract static class Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4, ATTR5> {

        private final ImmutableBsonConverterFactory4.Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> parent;
        private final ImmutableAttributeFactory<OBJ, ATTR5> attributeFactory;

        protected Layer(ImmutableBsonConverterFactory4.Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> parent, ImmutableAttributeFactory<OBJ, ATTR5> attributeFactory) {
            this.parent = requireNonNull(parent);
            this.attributeFactory = requireNonNull(attributeFactory);
        }

        public ImmutableBsonConverterFactory5<OBJ, ATTR1, ATTR2, ATTR3, ATTR4, ATTR5> constructedBy(ImmutableBsonConverter5.Constructor<OBJ, ATTR1, ATTR2, ATTR3, ATTR4, ATTR5> constructor) {
            return new ImmutableBsonConverterFactory5<>(this, constructor);
        }

        public ImmutableBsonConverter5.Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4, ATTR5> create(ConverterRegistry registry) {
            return new ImmutableBsonConverter5.Layer<>(parent.create(registry), attributeFactory.create(registry).build());
        }

        public Class<OBJ> objectClass() {
            return parent.objectClass();
        }

        public static final class WithId<OBJ, ATTR1, ATTR2, ATTR3, ATTR4, ATTR5> extends Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4, ATTR5> {

            public WithId(ImmutableBsonConverterFactory4.Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> parent, ImmutableAttributeFactory<OBJ, ATTR5> attribute) {
                super(parent, attribute);
            }
        }

        public static final class WithoutId<OBJ, ATTR1, ATTR2, ATTR3, ATTR4, ATTR5> extends Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4, ATTR5> {

            public WithoutId(ImmutableBsonConverterFactory4.Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> parent, ImmutableAttributeFactory<OBJ, ATTR5> attribute) {
                super(parent, attribute);
            }
        }
    }

}
