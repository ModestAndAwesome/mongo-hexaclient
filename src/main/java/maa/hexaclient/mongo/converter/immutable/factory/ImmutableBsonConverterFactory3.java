package maa.hexaclient.mongo.converter.immutable.factory;

import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import maa.hexaclient.mongo.converter.ObjectBsonConverterFactory;
import maa.hexaclient.mongo.converter.immutable.ImmutableBsonConverter3;
import maa.hexaclient.mongo.converter.immutable.attribute.ImmutableAttributeFactory;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;

import static java.util.Objects.requireNonNull;

public final class ImmutableBsonConverterFactory3<OBJ, ATTR1, ATTR2, ATTR3> implements ObjectBsonConverterFactory<OBJ> {
    private final Layer<OBJ, ATTR1, ATTR2, ATTR3> parent;
    private final ImmutableBsonConverter3.Constructor<OBJ, ATTR1, ATTR2, ATTR3> constructor;

    public ImmutableBsonConverterFactory3(Layer<OBJ, ATTR1, ATTR2, ATTR3> parent, ImmutableBsonConverter3.Constructor<OBJ, ATTR1, ATTR2, ATTR3> constructor) {
        this.parent = requireNonNull(parent);
        this.constructor = requireNonNull(constructor);
    }

    @Override
    public ObjectBsonConverter<OBJ> create(ConverterRegistry registry) {
        return new ImmutableBsonConverter3<>(parent.create(registry), constructor);
    }

    @Override
    public Class<OBJ> objectClass() {
        return parent.objectClass();
    }

    public abstract static class Layer<OBJ, ATTR1, ATTR2, ATTR3> {

        private final ImmutableBsonConverterFactory2.Layer<OBJ, ATTR1, ATTR2> parent;
        private final ImmutableAttributeFactory<OBJ, ATTR3> attributeFactory;

        protected Layer(ImmutableBsonConverterFactory2.Layer<OBJ, ATTR1, ATTR2> parent, ImmutableAttributeFactory<OBJ, ATTR3> attributeFactory) {
            this.parent = requireNonNull(parent);
            this.attributeFactory = requireNonNull(attributeFactory);
        }

        public ImmutableBsonConverterFactory3<OBJ, ATTR1, ATTR2, ATTR3> constructedBy(ImmutableBsonConverter3.Constructor<OBJ, ATTR1, ATTR2, ATTR3> constructor) {
            return new ImmutableBsonConverterFactory3<>(this, constructor);
        }

        public ImmutableBsonConverter3.Layer<OBJ, ATTR1, ATTR2, ATTR3> create(ConverterRegistry registry) {
            return new ImmutableBsonConverter3.Layer<>(parent.create(registry), attributeFactory.create(registry).build());
        }

        public Class<OBJ> objectClass() {
            return parent.objectClass();
        }

        public static final class WithId<OBJ, ATTR1, ATTR2, ATTR3> extends Layer<OBJ, ATTR1, ATTR2, ATTR3> {

            public WithId(ImmutableBsonConverterFactory2.Layer<OBJ, ATTR1, ATTR2> parent, ImmutableAttributeFactory<OBJ, ATTR3> attribute) {
                super(parent, attribute);
            }

            public <ATTR4> ImmutableBsonConverterFactory4.Layer.WithId<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> withAttribute(ImmutableAttributeFactory.Standard<OBJ, ATTR4> attribute) {
                return new ImmutableBsonConverterFactory4.Layer.WithId<>(this, attribute);
            }
        }

        public static final class WithoutId<OBJ, ATTR1, ATTR2, ATTR3> extends Layer<OBJ, ATTR1, ATTR2, ATTR3> {

            public WithoutId(ImmutableBsonConverterFactory2.Layer<OBJ, ATTR1, ATTR2> parent, ImmutableAttributeFactory<OBJ, ATTR3> attribute) {
                super(parent, attribute);
            }

            public <ATTR4> ImmutableBsonConverterFactory4.Layer.WithId<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> withId(ImmutableAttributeFactory.Id<OBJ, ATTR4> idAttribute) {
                return new ImmutableBsonConverterFactory4.Layer.WithId<>(this, idAttribute);
            }

            public <ATTR4> ImmutableBsonConverterFactory4.Layer.WithoutId<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> withAttribute(ImmutableAttributeFactory.Standard<OBJ, ATTR4> attribute) {
                return new ImmutableBsonConverterFactory4.Layer.WithoutId<>(this, attribute);
            }
        }
    }

}
