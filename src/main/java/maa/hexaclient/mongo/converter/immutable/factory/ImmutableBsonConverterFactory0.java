package maa.hexaclient.mongo.converter.immutable.factory;

import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import maa.hexaclient.mongo.converter.ObjectBsonConverterFactory;
import maa.hexaclient.mongo.converter.immutable.ImmutableBsonConverter0;
import maa.hexaclient.mongo.converter.immutable.attribute.ImmutableAttributeFactory;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;

import static java.util.Objects.requireNonNull;

public final class ImmutableBsonConverterFactory0<OBJ> implements ObjectBsonConverterFactory<OBJ> {
    private final Layer<OBJ> parent;
    private final ImmutableBsonConverter0.Constructor<OBJ> constructor;

    public ImmutableBsonConverterFactory0(Layer<OBJ> parent, ImmutableBsonConverter0.Constructor<OBJ> constructor) {
        this.parent = requireNonNull(parent);
        this.constructor = requireNonNull(constructor);
    }

    @Override
    public ObjectBsonConverter<OBJ> create(ConverterRegistry registry) {
        return new ImmutableBsonConverter0<>(parent.create(), constructor);
    }

    @Override
    public Class<OBJ> objectClass() {
        return parent.objectClass();
    }

    public static final class Layer<OBJ> {

        private final Class<OBJ> objectClass;

        public Layer(Class<OBJ> objectClass) {
            this.objectClass = requireNonNull(objectClass);
        }

        public Class<OBJ> objectClass() {
            return objectClass;
        }

        public <ATTR1> ImmutableBsonConverterFactory1.Layer.WithId<OBJ, ATTR1> withId(ImmutableAttributeFactory.Id<OBJ, ATTR1> factory) {
            return new ImmutableBsonConverterFactory1.Layer.WithId<>(this, factory);
        }

        public <ATTR1> ImmutableBsonConverterFactory1.Layer.WithoutId<OBJ, ATTR1> withAttribute(ImmutableAttributeFactory.Standard<OBJ, ATTR1> factory) {
            return new ImmutableBsonConverterFactory1.Layer.WithoutId<>(this, factory);
        }

        public ImmutableBsonConverterFactory0<OBJ> constructedBy(ImmutableBsonConverter0.Constructor<OBJ> constructor) {
            return new ImmutableBsonConverterFactory0<>(this, constructor);
        }

        public ImmutableBsonConverter0.Layer<OBJ> create() {
            return new ImmutableBsonConverter0.Layer<>(objectClass);
        }
    }

}
