package maa.hexaclient.mongo.converter.immutable.attribute;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.ConversionException;
import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonDocument;
import org.bson.BsonValue;

import java.util.Optional;

import static java.util.Objects.requireNonNull;

/**
 * An immutable attribute mapping. Allow to bind an attribute of an object to one or several field in a bson document
 * @param <OBJ>
 * @param <ATTR>
 */
public interface ImmutableAttribute<OBJ, ATTR> {

    BsonDocument appendTo(BsonDocument bsonDocument, OBJ object, ConverterContext context);

    Optional<ATTR> retrieveValue(BsonDocument bsonDocument, ConverterContext context);

    final class WithFallback<OBJ, ATTR> implements ImmutableAttribute<OBJ, ATTR> {

        private final ImmutableAttribute<OBJ, ATTR> delegate;
        private final String fallbackAttributeName;
        private final BsonConverter<ATTR> codec;

        public WithFallback(ImmutableAttribute<OBJ, ATTR> delegate, String fallbackAttributeName, BsonConverter<ATTR> codec) {
            this.delegate = requireNonNull(delegate);
            this.fallbackAttributeName = requireNonNull(fallbackAttributeName);
            this.codec = requireNonNull(codec);
        }

        @Override
        public BsonDocument appendTo(BsonDocument bsonDocument, OBJ object, ConverterContext context) {
            return delegate.appendTo(bsonDocument, object, context);
        }

        @Override
        public Optional<ATTR> retrieveValue(BsonDocument bsonDocument, ConverterContext context) {
            return delegate.retrieveValue(bsonDocument, context)
                    .or(() -> Optional.ofNullable(bsonDocument.get(fallbackAttributeName))
                            .map((BsonValue bson) -> codec.toObject(bson, context)));
        }
    }

    final class WithDefault<OBJ, ATTR> implements ImmutableAttribute<OBJ, ATTR> {

        private final ImmutableAttribute<OBJ, ATTR> delegate;
        private final ATTR defaultValue;

        public WithDefault(ImmutableAttribute<OBJ, ATTR> delegate, ATTR defaultValue) {
            this.delegate = requireNonNull(delegate);
            this.defaultValue = requireNonNull(defaultValue);
        }

        @Override
        public BsonDocument appendTo(BsonDocument bsonDocument, OBJ object, ConverterContext context) {
            return delegate.appendTo(bsonDocument, object, context);
        }

        @Override
        public Optional<ATTR> retrieveValue(BsonDocument bsonDocument, ConverterContext context) {
            return delegate.retrieveValue(bsonDocument, context).or(() -> Optional.of(defaultValue));
        }
    }

    final class Mandatory<OBJ, ATTR> implements ImmutableAttribute<OBJ, ATTR> {

        private final ImmutableAttribute<OBJ, ATTR> delegate;

        public Mandatory(ImmutableAttribute<OBJ, ATTR> delegate) {
            this.delegate = requireNonNull(delegate);
        }

        @Override
        public BsonDocument appendTo(BsonDocument bsonDocument, OBJ object, ConverterContext context) {
            return delegate.appendTo(bsonDocument, object, context);
        }

        @Override
        public Optional<ATTR> retrieveValue(BsonDocument bsonDocument, ConverterContext context) {
            final Optional<ATTR> optionalValue = delegate.retrieveValue(bsonDocument, context);

            if (optionalValue.isEmpty()) {
                throw new ConversionException("Value not set for %s".formatted(delegate));
            }

            return optionalValue;
        }
    }

}
