package maa.hexaclient.mongo.converter.immutable.attribute;

public interface ImmutableAttributeBuilder<OBJ, ATTR> {

    ImmutableAttribute<OBJ, ATTR> build();

}
