package maa.hexaclient.mongo.converter.immutable.attribute;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.BsonConverterFactory;
import maa.hexaclient.mongo.converter.ConversionException;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;

import java.util.function.Function;

import static java.util.Objects.requireNonNull;

public final class ImmutableObjectAttributeBuilder<OBJ, ATTR> implements ImmutableAttributeBuilder<OBJ, ATTR> {

    private final Function<OBJ, ATTR> extractor;
    private final String name;
    private final BsonConverter<ATTR> converter;

    private String fallback = null;
    private NullHandler<ATTR> nullHandler = NullHandler.nullable();


    public ImmutableObjectAttributeBuilder(Function<OBJ, ATTR> extractor, String name, BsonConverter<ATTR> converter) {
        this.extractor = requireNonNull(extractor);
        this.name = requireNonNull(name);
        this.converter = requireNonNull(converter);
    }

    public static <OBJ, ATTR> ImmutableObjectAttributeBuilder.Step1<OBJ, ATTR> from(ConverterRegistry registry, Function<OBJ, ATTR> extractor) {
        return new ImmutableObjectAttributeBuilder.Step1<>(registry, extractor);
    }

    @Override
    public ImmutableAttribute<OBJ, ATTR> build() {

        ImmutableAttribute<OBJ, ATTR> attribute = new ImmutableObjectAttribute<>(name,
                nullHandler.addNullHandle(extractor),
                converter);

        if (fallback != null) {
            attribute = new ImmutableAttribute.WithFallback<>(attribute, fallback, converter);
        }

        return nullHandler.addNullHandle(attribute);
    }

    public ImmutableObjectAttributeBuilder<OBJ, ATTR> fallback(String attrName) {
        this.fallback = attrName;
        return this;
    }

    public ImmutableObjectAttributeBuilder<OBJ, ATTR> mandatory() {
        return mandatory(Direction.BOTH);
    }

    public ImmutableObjectAttributeBuilder<OBJ, ATTR> mandatory(Direction direction) {
        this.nullHandler = NullHandler.mandatory(name, direction);
        return this;
    }

    public ImmutableObjectAttributeBuilder<OBJ, ATTR> defaultValue(ATTR defaultValue) {
        return defaultValue(defaultValue, Direction.BOTH);
    }

    public ImmutableObjectAttributeBuilder<OBJ, ATTR> defaultValue(ATTR defaultValue, Direction direction) {
        this.nullHandler = NullHandler.withDefault(defaultValue, direction);
        return this;
    }

    public ImmutableObjectAttributeBuilder<OBJ, ATTR> nullable() {
        this.nullHandler = NullHandler.nullable();
        return this;
    }

    public static final class Step1<OBJ, ATTR> {

        private final ConverterRegistry registry;
        private final Function<OBJ, ATTR> extractor;

        public Step1(ConverterRegistry registry, Function<OBJ, ATTR> extractor) {
            this.registry = requireNonNull(registry);
            this.extractor = requireNonNull(extractor);
        }

        public Step2<OBJ, ATTR> to(String attributeName) {
            return new Step2<>(registry, extractor, attributeName);
        }
    }

    public static final class Step2<OBJ, ATTR> {

        private final ConverterRegistry registry;
        private final Function<OBJ, ATTR> extractor;
        private final String name;

        private Step2(ConverterRegistry registry, Function<OBJ, ATTR> extractor, String name) {
            this.registry = requireNonNull(registry);
            this.extractor = requireNonNull(extractor);
            this.name = requireNonNull(name);
        }

        public ImmutableObjectAttributeBuilder<OBJ, ATTR> as(Class<ATTR> attributeClass) {

            final BsonConverter<ATTR> codec = registry.getConverterFor(attributeClass)
                    .orElseThrow(() -> new IllegalStateException("No converter found for class %s".formatted(attributeClass)));
            return this.using(codec);
        }

        public ImmutableObjectAttributeBuilder<OBJ, ATTR> using(final BsonConverterFactory<ATTR> bsonConverterFactory) {
            return using(bsonConverterFactory.create(registry));
        }

        public ImmutableObjectAttributeBuilder<OBJ, ATTR> using(final BsonConverter<ATTR> converter) {

            return new ImmutableObjectAttributeBuilder<>(
                    extractor,
                    name,
                    converter);
        }
    }

    public enum Direction {
        READ(true, false),
        WRITE(false, true),
        BOTH(true, true);

        public final boolean onRead;
        public final boolean onWrite;

        Direction(boolean onRead, boolean onWrite) {
            this.onRead = onRead;
            this.onWrite = onWrite;
        }
    }

    private interface NullHandler<ATTR> {

        static <ATTR> NullHandler<ATTR> nullable() {
            return new Nullable<>();

        }
        static <ATTR> NullHandler<ATTR> withDefault(ATTR defaultValue, Direction direction) {
            return new WithDefault<>(defaultValue, direction);
        }

        static <ATTR> NullHandler<ATTR> mandatory(String fieldIdentification, Direction direction) {
            return new Mandatory<>(fieldIdentification, direction);
        }

        <OBJ> ImmutableAttribute<OBJ, ATTR> addNullHandle(ImmutableAttribute<OBJ, ATTR> attribute);

        <OBJ> Function<OBJ, ATTR> addNullHandle(Function<OBJ, ATTR> extractor);

        class Nullable<ATTR> implements NullHandler<ATTR> {

            @Override
            public <OBJ> ImmutableAttribute<OBJ, ATTR> addNullHandle(ImmutableAttribute<OBJ, ATTR> attribute) {
                return attribute;
            }

            @Override
            public <OBJ> Function<OBJ, ATTR> addNullHandle(Function<OBJ, ATTR> extractor) {
                return extractor;
            }
        }

        class WithDefault<ATTR> implements NullHandler<ATTR> {

            private final ATTR defaultValue;
            private final Direction direction;

            public WithDefault(ATTR defaultValue, Direction direction) {
                this.defaultValue = requireNonNull(defaultValue);
                this.direction = requireNonNull(direction);
            }

            @Override
            public <OBJ> ImmutableAttribute<OBJ, ATTR> addNullHandle(ImmutableAttribute<OBJ, ATTR> attribute) {
                return direction.onRead ? new ImmutableAttribute.WithDefault<>(attribute, defaultValue) : attribute;
            }

            @Override
            public <OBJ> Function<OBJ, ATTR> addNullHandle(Function<OBJ, ATTR> extractor) {
                return direction.onWrite ? extractor.andThen(attr -> attr == null ? defaultValue : attr) : extractor;
            }
        }

        class Mandatory<ATTR> implements NullHandler<ATTR> {

            private final String fieldIdentification;
            private final Direction direction;

            public Mandatory(String fieldIdentification, Direction direction) {
                this.fieldIdentification = fieldIdentification;
                this.direction = direction;
            }

            @Override
            public <OBJ> ImmutableAttribute<OBJ, ATTR> addNullHandle(ImmutableAttribute<OBJ, ATTR> attribute) {
                return new ImmutableAttribute.Mandatory<>(attribute);
            }

            @Override
            public <OBJ> Function<OBJ, ATTR> addNullHandle(Function<OBJ, ATTR> extractor) {
                return direction.onWrite ? extractor.andThen(attr -> {
                    if (attr == null) throw new ConversionException("Field %s cannot be null".formatted(fieldIdentification));
                    return attr;
                }) : extractor;
            }
        }
    }
}
