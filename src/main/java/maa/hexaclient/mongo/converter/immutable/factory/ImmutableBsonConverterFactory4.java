package maa.hexaclient.mongo.converter.immutable.factory;

import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import maa.hexaclient.mongo.converter.ObjectBsonConverterFactory;
import maa.hexaclient.mongo.converter.immutable.ImmutableBsonConverter4;
import maa.hexaclient.mongo.converter.immutable.attribute.ImmutableAttributeFactory;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;

import static java.util.Objects.requireNonNull;

public final class ImmutableBsonConverterFactory4<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> implements ObjectBsonConverterFactory<OBJ> {
    private final Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> parent;
    private final ImmutableBsonConverter4.Constructor<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> constructor;

    public ImmutableBsonConverterFactory4(
            Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> parent,
            ImmutableBsonConverter4.Constructor<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> constructor
    ) {
        this.parent = requireNonNull(parent);
        this.constructor = requireNonNull(constructor);
    }

    @Override
    public ObjectBsonConverter<OBJ> create(ConverterRegistry registry) {
        return new ImmutableBsonConverter4<>(parent.create(registry), constructor);
    }

    @Override
    public Class<OBJ> objectClass() {
        return parent.objectClass();
    }

    public abstract static class Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> {

        private final ImmutableBsonConverterFactory3.Layer<OBJ, ATTR1, ATTR2, ATTR3> parent;
        private final ImmutableAttributeFactory<OBJ, ATTR4> attributeFactory;

        protected Layer(ImmutableBsonConverterFactory3.Layer<OBJ, ATTR1, ATTR2, ATTR3> parent, ImmutableAttributeFactory<OBJ, ATTR4> attributeFactory) {
            this.parent = requireNonNull(parent);
            this.attributeFactory = requireNonNull(attributeFactory);
        }

        public ImmutableBsonConverterFactory4<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> constructedBy(ImmutableBsonConverter4.Constructor<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> constructor) {
            return new ImmutableBsonConverterFactory4<>(this, constructor);
        }

        public ImmutableBsonConverter4.Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> create(ConverterRegistry registry) {
            return new ImmutableBsonConverter4.Layer<>(parent.create(registry), attributeFactory.create(registry).build());
        }

        public Class<OBJ> objectClass() {
            return parent.objectClass();
        }

        public static final class WithId<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> extends Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> {

            public WithId(ImmutableBsonConverterFactory3.Layer<OBJ, ATTR1, ATTR2, ATTR3> parent, ImmutableAttributeFactory<OBJ, ATTR4> attribute) {
                super(parent, attribute);
            }

            public <ATTR5> ImmutableBsonConverterFactory5.Layer.WithId<OBJ, ATTR1, ATTR2, ATTR3, ATTR4, ATTR5> withAttribute(ImmutableAttributeFactory.Standard<OBJ, ATTR5> attribute) {
                return new ImmutableBsonConverterFactory5.Layer.WithId<>(this, attribute);
            }
        }

        public static final class WithoutId<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> extends Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> {

            public WithoutId(ImmutableBsonConverterFactory3.Layer<OBJ, ATTR1, ATTR2, ATTR3> parent, ImmutableAttributeFactory<OBJ, ATTR4> attribute) {
                super(parent, attribute);
            }

            public <ATTR5> ImmutableBsonConverterFactory5.Layer.WithId<OBJ, ATTR1, ATTR2, ATTR3, ATTR4, ATTR5> withId(ImmutableAttributeFactory.Id<OBJ, ATTR5> idAttribute) {
                return new ImmutableBsonConverterFactory5.Layer.WithId<>(this, idAttribute);
            }

            public <ATTR5> ImmutableBsonConverterFactory5.Layer.WithoutId<OBJ, ATTR1, ATTR2, ATTR3, ATTR4, ATTR5> withAttribute(ImmutableAttributeFactory.Standard<OBJ, ATTR5> attribute) {
                return new ImmutableBsonConverterFactory5.Layer.WithoutId<>(this, attribute);
            }
        }
    }

}
