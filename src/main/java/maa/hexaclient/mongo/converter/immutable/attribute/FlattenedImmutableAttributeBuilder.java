package maa.hexaclient.mongo.converter.immutable.attribute;

import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import maa.hexaclient.mongo.converter.ObjectBsonConverterFactory;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;

import java.util.function.Function;

import static java.util.Objects.requireNonNull;

public final class FlattenedImmutableAttributeBuilder<OBJ, ATTR> {

    private final Function<OBJ, ATTR> extractor;
    private final ObjectBsonConverter<ATTR> converter;

    public FlattenedImmutableAttributeBuilder(Function<OBJ, ATTR> extractor, ObjectBsonConverter<ATTR> converter) {
        this.extractor = requireNonNull(extractor);
        this.converter = requireNonNull(converter);
    }

    public static <ATTR, OBJ> Step1<OBJ, ATTR> from(ConverterRegistry registry, Function<OBJ, ATTR> extractor) {
        return new Step1<>(registry, extractor);
    }

    public FlattenedImmutableAttribute<OBJ, ATTR> build() {
        return new FlattenedImmutableAttribute<>(extractor, converter);
    }

    public static class Step1<OBJ, ATTR> {

        private final ConverterRegistry registry;
        private final Function<OBJ, ATTR> extractor;

        public Step1(ConverterRegistry registry, Function<OBJ, ATTR> extractor) {
            this.registry = requireNonNull(registry);
            this.extractor = requireNonNull(extractor);
        }

        public FlattenedImmutableAttributeBuilder<OBJ, ATTR> withConverter(ObjectBsonConverterFactory<ATTR> converterFactory) {
            return new FlattenedImmutableAttributeBuilder<>(extractor, converterFactory.create(registry));
        }

        public FlattenedImmutableAttributeBuilder<OBJ, ATTR> withConverter(ObjectBsonConverter<ATTR> converter) {
            return new FlattenedImmutableAttributeBuilder<>(extractor, converter);
        }
    }
}
