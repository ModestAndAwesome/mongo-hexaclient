package maa.hexaclient.mongo.converter.immutable.attribute;

import maa.hexaclient.mongo.converter.registry.ConverterRegistry;

@FunctionalInterface
public interface ImmutableAttributeFactory<OBJ, ATTR> {

	ImmutableAttributeBuilder<OBJ, ATTR> create(ConverterRegistry converterRegistry);

	@FunctionalInterface
	interface Standard<OBJ, ATTR> extends ImmutableAttributeFactory<OBJ, ATTR> {
		ImmutableAttributeBuilder<OBJ, ATTR> create(ImmutableAttributeInitier.Standard initier);

		@Override
		default ImmutableAttributeBuilder<OBJ, ATTR> create(ConverterRegistry converterRegistry) {
			return this.create(ImmutableAttributeInitier.standard(converterRegistry));
		}
	}

	@FunctionalInterface
	interface Id<OBJ, ATTR> extends ImmutableAttributeFactory<OBJ, ATTR> {
		ImmutableAttributeBuilder<OBJ, ATTR> create(ImmutableAttributeInitier.Id initier);

		@Override
		default ImmutableAttributeBuilder<OBJ, ATTR> create(ConverterRegistry converterRegistry) {
			return this.create(ImmutableAttributeInitier.id(converterRegistry));
		}
	}

}
