package maa.hexaclient.mongo.converter.immutable;

import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import org.bson.BsonDocument;

import static java.util.Objects.requireNonNull;

public final class ImmutableBsonConverter0<OBJ> implements ObjectBsonConverter<OBJ> {

    private final Layer<OBJ> parent;
    private final Constructor<OBJ> constructor;


    public ImmutableBsonConverter0(Layer<OBJ> parent, Constructor<OBJ> constructor) {
        this.parent = requireNonNull(parent);
        this.constructor = requireNonNull(constructor);
    }

    @Override
    public BsonDocument toBson(OBJ object, ConverterContext context) {
        requireNonNull(object);
        requireNonNull(context);

        return parent.toBson();
    }

    @Override
    public OBJ toObject(BsonDocument bsonDocument, ConverterContext context) {
        return constructor.construct();
    }

    @Override
    public Class<OBJ> convertedClass() {
        return parent.convertedClass();
    }

    public static final class Layer<OBJ> {

        private final Class<OBJ> convertedClass;

        public Layer(Class<OBJ> convertedClass) {
            this.convertedClass = requireNonNull(convertedClass);
        }

        public BsonDocument toBson() {
            return new BsonDocument();
        }

        public Class<OBJ> convertedClass() {
            return convertedClass;
        }
    }

    public interface Constructor<OBJ> {
        OBJ construct();
    }
}
