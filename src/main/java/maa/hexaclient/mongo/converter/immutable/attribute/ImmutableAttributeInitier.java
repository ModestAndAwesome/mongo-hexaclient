package maa.hexaclient.mongo.converter.immutable.attribute;

import maa.hexaclient.mongo.converter.registry.ConverterRegistry;

import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

public interface ImmutableAttributeInitier {

	static Standard standard(ConverterRegistry registry) {
		return new Standard(registry);
	}

	static Id id(ConverterRegistry registry) {
		return new Id(registry);
	}

	class Standard {
		private final ConverterRegistry registry;

		public Standard(ConverterRegistry registry) {
			this.registry = requireNonNull(registry);
		}

		public <OBJ, ATTR> ImmutableObjectAttributeBuilder.Step1<OBJ, ATTR> from(Function<OBJ, ATTR> extractor) {
			return ImmutableObjectAttributeBuilder.from(registry, extractor);
		}

		public <OBJ, ATTR> ImmutableIterableAttributeBuilder.Step1<OBJ, List<ATTR>, ATTR> fromList(Function<OBJ, List<ATTR>> extractor) {
			return new ImmutableIterableAttributeBuilder.Step1<>(registry, extractor, Collectors.toList());
		}

		public <OBJ, ATTR> ImmutableIterableAttributeBuilder.Step1<OBJ, Set<ATTR>, ATTR> fromSet(Function<OBJ, Set<ATTR>> extractor) {
			return new ImmutableIterableAttributeBuilder.Step1<>(registry, extractor, Collectors.toSet());
		}

		public <OBJ, ATTR> FlattenedImmutableAttributeBuilder.Step1<OBJ, ATTR> flattening(Function<OBJ, ATTR> extractor) {
			return FlattenedImmutableAttributeBuilder.from(registry, extractor);
		}
	}

	class Id {
		private final ConverterRegistry registry;

		public Id(ConverterRegistry registry) {
			this.registry = requireNonNull(registry);
		}

		public <OBJ, ATTR> ImmutableObjectAttributeBuilder.Step2<OBJ, ATTR> from(Function<OBJ, ATTR> extractor) {
			return ImmutableObjectAttributeBuilder.from(registry, extractor).to("_id");
		}
	}
}
