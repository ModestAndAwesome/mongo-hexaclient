package maa.hexaclient.mongo.converter.immutable;

import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import maa.hexaclient.mongo.converter.immutable.attribute.ImmutableAttribute;
import org.bson.BsonDocument;

import static java.util.Objects.requireNonNull;

public record ImmutableBsonConverter3<OBJ, ATTR1, ATTR2, ATTR3>(
        ImmutableBsonConverter3.Layer<OBJ, ATTR1, ATTR2, ATTR3> parent,
        ImmutableBsonConverter3.Constructor<OBJ, ATTR1, ATTR2, ATTR3> constructor
) implements ObjectBsonConverter<OBJ> {

    public ImmutableBsonConverter3(Layer<OBJ, ATTR1, ATTR2, ATTR3> parent, Constructor<OBJ, ATTR1, ATTR2, ATTR3> constructor) {
        this.parent = requireNonNull(parent);
        this.constructor = requireNonNull(constructor);
    }

    @Override
    public BsonDocument toBson(OBJ object, ConverterContext context) {
        requireNonNull(object);
        requireNonNull(context);

        return parent.toBson(object, context);
    }

    @Override
    public OBJ toObject(BsonDocument bsonDocument, ConverterContext context) {
        return parent.toObject(bsonDocument, context, constructor);
    }

    @Override
    public Class<OBJ> convertedClass() {
        return parent.convertedClass();
    }

    public static final class Layer<OBJ, ATTR1, ATTR2, ATTR3> {
        private final ImmutableBsonConverter2.Layer<OBJ, ATTR1, ATTR2> parent;
        private final ImmutableAttribute<OBJ, ATTR3> attribute;

        public Layer(ImmutableBsonConverter2.Layer<OBJ, ATTR1, ATTR2> parent, ImmutableAttribute<OBJ, ATTR3> attribute) {
            this.parent = requireNonNull(parent);
            this.attribute = requireNonNull(attribute);
        }

        public BsonDocument toBson(OBJ object, ConverterContext context) {
            return attribute.appendTo(parent.toBson(object, context), object, context);
        }

        public OBJ toObject(BsonDocument bsonDocument, ConverterContext context, Constructor<OBJ, ATTR1, ATTR2, ATTR3> constructor) {
            return parent.toObject(bsonDocument,
                    context,
                    (attr1, attr2) ->
                            constructor.construct(attr1, attr2, attribute.retrieveValue(bsonDocument, context).orElse(null)));
        }

        public Class<OBJ> convertedClass() {
            return parent.convertedClass();
        }

        public ImmutableBsonConverter2.Layer<OBJ, ATTR1, ATTR2> parent() {
            return parent;
        }

        public ImmutableAttribute<OBJ, ATTR3> attribute() {
            return attribute;
        }
    }

    public interface Constructor<OBJ, ATTR1, ATTR2, ATTR3> {
        OBJ construct(ATTR1 attr1, ATTR2 attr2, ATTR3 attr3);
    }
}
