package maa.hexaclient.mongo.converter.immutable.attribute;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.BsonConverterFactory;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;

import java.util.function.Function;
import java.util.stream.Collector;

import static java.util.Objects.requireNonNull;

@SuppressWarnings("ClassCanBeRecord")
public class ImmutableIterableAttributeBuilder<OBJ, IT extends Iterable<ATTR>, ATTR> implements ImmutableAttributeBuilder<OBJ, IT> {

    private final Function<OBJ, IT> extractor;
    private final Collector<ATTR, ?, IT> collector;
    private final BsonConverter<ATTR> converter;

    private final String name;

    public ImmutableIterableAttributeBuilder(Function<OBJ, IT> extractor, Collector<ATTR, ?, IT> collector, BsonConverter<ATTR> converter, String name) {
        this.extractor = extractor;
        this.collector = collector;
        this.converter = converter;
        this.name = name;
    }

    @Override
    public ImmutableAttribute<OBJ, IT> build() {
        return new ImmutableIterableAttribute<>(name, extractor, converter, collector);
    }


    public static final class Step1<OBJ, IT extends Iterable<ATTR>, ATTR> {

        private final ConverterRegistry registry;
        private final Function<OBJ, IT> extractor;
        private final Collector<ATTR, ?, IT> collector;

        public Step1(ConverterRegistry registry, Function<OBJ, IT> extractor, Collector<ATTR, ?, IT> collector) {
            this.registry = requireNonNull(registry);
            this.extractor = requireNonNull(extractor);
            this.collector = requireNonNull(collector);
        }

        public ImmutableIterableAttributeBuilder.Step2<OBJ, IT, ATTR> to(String attributeName) {
            return new ImmutableIterableAttributeBuilder.Step2<>(registry, extractor, collector, attributeName);
        }
    }

    public static final class Step2<OBJ, IT extends Iterable<ATTR>, ATTR> {

        private final ConverterRegistry registry;
        private final Function<OBJ, IT> extractor;
        private final Collector<ATTR, ?, IT> collector;
        private final String name;

        public Step2(ConverterRegistry registry, Function<OBJ, IT> extractor, Collector<ATTR, ?, IT> collector, String name) {
            this.registry = registry;
            this.extractor = extractor;
            this.collector = collector;
            this.name = name;
        }

        public ImmutableIterableAttributeBuilder<OBJ, IT, ATTR> itemsAs(Class<ATTR> attributeClass) {
            return itemUsing(registry.getMandatoryConverterFor(attributeClass));
        }

        public ImmutableIterableAttributeBuilder<OBJ, IT, ATTR> itemUsing(BsonConverterFactory<ATTR> converterFactory) {
            return new ImmutableIterableAttributeBuilder<>(extractor, collector, converterFactory.create(registry), name);
        }

        public ImmutableIterableAttributeBuilder<OBJ, IT, ATTR> itemUsing(BsonConverter<ATTR> converter) {
            return new ImmutableIterableAttributeBuilder<>(extractor, collector, converter, name);
        }
    }
}
