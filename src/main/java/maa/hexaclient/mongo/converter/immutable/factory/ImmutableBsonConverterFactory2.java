package maa.hexaclient.mongo.converter.immutable.factory;

import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import maa.hexaclient.mongo.converter.ObjectBsonConverterFactory;
import maa.hexaclient.mongo.converter.immutable.ImmutableBsonConverter2;
import maa.hexaclient.mongo.converter.immutable.attribute.ImmutableAttributeFactory;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;

import static java.util.Objects.requireNonNull;

public final class ImmutableBsonConverterFactory2<OBJ, ATTR1, ATTR2> implements ObjectBsonConverterFactory<OBJ> {
    private final Layer<OBJ, ATTR1, ATTR2> parent;
    private final ImmutableBsonConverter2.Constructor<OBJ, ATTR1, ATTR2> constructor;

    public ImmutableBsonConverterFactory2(Layer<OBJ, ATTR1, ATTR2> parent, ImmutableBsonConverter2.Constructor<OBJ, ATTR1, ATTR2> constructor) {
        this.parent = requireNonNull(parent);
        this.constructor = requireNonNull(constructor);
    }

    @Override
    public ObjectBsonConverter<OBJ> create(ConverterRegistry registry) {
        return new ImmutableBsonConverter2<>(parent.create(registry), constructor);
    }

    @Override
    public Class<OBJ> objectClass() {
        return parent.objectClass();
    }

    public abstract static class Layer<OBJ, ATTR1, ATTR2> {

        private final ImmutableBsonConverterFactory1.Layer<OBJ, ATTR1> parent;
        private final ImmutableAttributeFactory<OBJ, ATTR2> attributeFactory;

        protected Layer(ImmutableBsonConverterFactory1.Layer<OBJ, ATTR1> parent, ImmutableAttributeFactory<OBJ, ATTR2> attributeFactory) {
            this.parent = requireNonNull(parent);
            this.attributeFactory = requireNonNull(attributeFactory);
        }

        public ImmutableBsonConverterFactory2<OBJ, ATTR1, ATTR2> constructedBy(ImmutableBsonConverter2.Constructor<OBJ, ATTR1, ATTR2> constructor) {
            return new ImmutableBsonConverterFactory2<>(this, constructor);
        }

        public ImmutableBsonConverter2.Layer<OBJ, ATTR1, ATTR2> create(ConverterRegistry registry) {
            return new ImmutableBsonConverter2.Layer<>(parent.create(registry), attributeFactory.create(registry).build());
        }

        public Class<OBJ> objectClass() {
            return parent.objectClass();
        }

        public static final class WithId<OBJ, ATTR1, ATTR2> extends Layer<OBJ, ATTR1, ATTR2> {

            public WithId(ImmutableBsonConverterFactory1.Layer<OBJ, ATTR1> parent, ImmutableAttributeFactory<OBJ, ATTR2> attribute) {
                super(parent, attribute);
            }

            public <ATTR3> ImmutableBsonConverterFactory3.Layer.WithId<OBJ, ATTR1, ATTR2, ATTR3> withAttribute(ImmutableAttributeFactory.Standard<OBJ, ATTR3> attribute2) {
                return new ImmutableBsonConverterFactory3.Layer.WithId<>(this, attribute2);
            }
        }

        public static final class WithoutId<OBJ, ATTR1, ATTR2> extends Layer<OBJ, ATTR1, ATTR2> {

            public WithoutId(ImmutableBsonConverterFactory1.Layer<OBJ, ATTR1> parent, ImmutableAttributeFactory<OBJ, ATTR2> attribute) {
                super(parent, attribute);
            }

            public <ATTR3> ImmutableBsonConverterFactory3.Layer.WithId<OBJ, ATTR1, ATTR2, ATTR3> withId(ImmutableAttributeFactory.Id<OBJ, ATTR3> factory) {
                return new ImmutableBsonConverterFactory3.Layer.WithId<>(this, factory);
            }

            public <ATTR3> ImmutableBsonConverterFactory3.Layer.WithoutId<OBJ, ATTR1, ATTR2, ATTR3> withAttribute(ImmutableAttributeFactory.Standard<OBJ, ATTR3> attribute) {
                return new ImmutableBsonConverterFactory3.Layer.WithoutId<>(this, attribute);
            }
        }
    }

}
