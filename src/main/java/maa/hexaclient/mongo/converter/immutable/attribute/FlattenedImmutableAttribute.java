package maa.hexaclient.mongo.converter.immutable.attribute;

import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import org.bson.BsonDocument;

import java.util.Optional;
import java.util.function.Function;

public final class FlattenedImmutableAttribute<OBJ, ATTR> implements ImmutableAttribute<OBJ, ATTR> {

    private final Function<OBJ, ATTR> extractor;
    private final ObjectBsonConverter<ATTR> attributeConverter;

    public FlattenedImmutableAttribute(Function<OBJ, ATTR> extractor, ObjectBsonConverter<ATTR> attributeConverter) {
        this.extractor = extractor;
        this.attributeConverter = attributeConverter;
    }

    @Override
    public BsonDocument appendTo(BsonDocument bsonDocument, OBJ object, ConverterContext context) {

        bsonDocument.putAll(attributeConverter.toBson(extractor.apply(object), context));
        return bsonDocument;
    }

    @Override
    public Optional<ATTR> retrieveValue(BsonDocument bsonDocument, ConverterContext context) {

        return Optional.of(attributeConverter.toObject(bsonDocument, context));
    }
}
