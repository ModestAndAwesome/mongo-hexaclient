package maa.hexaclient.mongo.converter.immutable.attribute;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonDocument;
import org.bson.BsonValue;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

public class ImmutableOptionalAttribute<OBJ, ATTR> implements ImmutableAttribute<OBJ, ATTR> {

    private final String name;
    private final Function<OBJ, Optional<ATTR>> extractor;
    private final BsonConverter<ATTR> attributeConverter;

    public ImmutableOptionalAttribute(String name, Function<OBJ, Optional<ATTR>> extractor, BsonConverter<ATTR> attributeConverter) {
        this.name = name;
        this.extractor = extractor;
        this.attributeConverter = attributeConverter;
    }

    @Override
    public BsonDocument appendTo(BsonDocument bsonDocument, OBJ object, ConverterContext context) {

        return extractor.apply(object)
                .map(value -> bsonDocument.append(name, attributeConverter.toBson(value, context)))
                .orElse(bsonDocument);
    }

    @Override
    public Optional<ATTR> retrieveValue(BsonDocument bsonDocument, ConverterContext context) {

        return Optional.ofNullable(bsonDocument.get(name))
                .filter(Predicate.not(BsonValue::isNull))
                .map(value -> attributeConverter.toObject(value, context));
    }
}
