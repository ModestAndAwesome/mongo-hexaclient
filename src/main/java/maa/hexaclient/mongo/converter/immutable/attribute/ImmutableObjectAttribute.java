package maa.hexaclient.mongo.converter.immutable.attribute;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonDocument;
import org.bson.BsonValue;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.Objects.requireNonNull;

public final class ImmutableObjectAttribute<OBJ, ATTR> implements ImmutableAttribute<OBJ, ATTR> {

    private final String name;
    private final Function<OBJ, ATTR> extractor;
    private final BsonConverter<ATTR> attributeConverter;

    public ImmutableObjectAttribute(String name, Function<OBJ, ATTR> extractor, BsonConverter<ATTR> attributeConverter) {
        this.name = requireNonNull(name);
        this.attributeConverter = requireNonNull(attributeConverter);
        this.extractor = requireNonNull(extractor);
    }

    @Override
    public BsonDocument appendTo(BsonDocument bsonDocument, OBJ object, ConverterContext context) {

        return Optional.ofNullable(extractor.apply(object))
                .map(value -> bsonDocument.append(name, attributeConverter.toBson(value, context)))
                .orElse(bsonDocument);
    }

    @Override
    public Optional<ATTR> retrieveValue(BsonDocument bsonDocument, ConverterContext context) {

        return Optional.ofNullable(bsonDocument.get(name))
                .filter(Predicate.not(BsonValue::isNull))
                .map(value -> attributeConverter.toObject(value, context));
    }
}
