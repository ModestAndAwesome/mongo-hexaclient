package maa.hexaclient.mongo.converter.immutable.factory;

import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import maa.hexaclient.mongo.converter.ObjectBsonConverterFactory;
import maa.hexaclient.mongo.converter.immutable.ImmutableBsonConverter1;
import maa.hexaclient.mongo.converter.immutable.attribute.ImmutableAttributeFactory;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;

import static java.util.Objects.requireNonNull;

public final class ImmutableBsonConverterFactory1<OBJ, ATTR1> implements ObjectBsonConverterFactory<OBJ> {
    private final Layer<OBJ, ATTR1> parent;
    private final ImmutableBsonConverter1.Constructor<OBJ, ATTR1> constructor;

    public ImmutableBsonConverterFactory1(Layer<OBJ, ATTR1> parent, ImmutableBsonConverter1.Constructor<OBJ, ATTR1> constructor) {
        this.parent = requireNonNull(parent);
        this.constructor = requireNonNull(constructor);
    }

    @Override
    public ObjectBsonConverter<OBJ> create(ConverterRegistry registry) {
        return new ImmutableBsonConverter1<>(parent.create(registry), constructor);
    }

    @Override
    public Class<OBJ> objectClass() {
        return parent.objectClass();
    }

    public abstract static class Layer<OBJ, ATTR1> {

        private final ImmutableBsonConverterFactory0.Layer<OBJ> parent;
        private final ImmutableAttributeFactory<OBJ, ATTR1> attributeFactory;

        protected Layer(ImmutableBsonConverterFactory0.Layer<OBJ> parent, ImmutableAttributeFactory<OBJ, ATTR1> attributeFactory) {
            this.parent = requireNonNull(parent);
            this.attributeFactory = requireNonNull(attributeFactory);
        }

        public Class<OBJ> objectClass() {
            return parent.objectClass();
        }

        ImmutableBsonConverter1.Layer<OBJ, ATTR1> create(ConverterRegistry registry) {
            return new ImmutableBsonConverter1.Layer<>(parent.create(), attributeFactory.create(registry).build());
        }

        public ImmutableBsonConverterFactory1<OBJ, ATTR1> constructedBy(ImmutableBsonConverter1.Constructor<OBJ, ATTR1> constructor) {
            return new ImmutableBsonConverterFactory1<>(this, constructor);
        }


        public static final class WithId<OBJ, ATTR1> extends Layer<OBJ, ATTR1> {

            public WithId(ImmutableBsonConverterFactory0.Layer<OBJ> parent, ImmutableAttributeFactory<OBJ, ATTR1> attribute) {
                super(parent, attribute);
            }

            public <ATTR2> ImmutableBsonConverterFactory2.Layer.WithId<OBJ, ATTR1, ATTR2> withAttribute(ImmutableAttributeFactory.Standard<OBJ, ATTR2> attribute2) {
                return new ImmutableBsonConverterFactory2.Layer.WithId<>(this, attribute2);
            }
        }


        public static final class WithoutId<OBJ, ATTR1> extends Layer<OBJ, ATTR1> {

            public WithoutId(ImmutableBsonConverterFactory0.Layer<OBJ> parent, ImmutableAttributeFactory<OBJ, ATTR1> attribute) {
                super(parent, attribute);
            }

            public <ATTR2> ImmutableBsonConverterFactory2.Layer.WithId<OBJ, ATTR1, ATTR2> withId(ImmutableAttributeFactory.Id<OBJ, ATTR2> idAttribute) {
                return new ImmutableBsonConverterFactory2.Layer.WithId<>(this, idAttribute);
            }

            public <ATTR2> ImmutableBsonConverterFactory2.Layer.WithoutId<OBJ, ATTR1, ATTR2> withAttribute(ImmutableAttributeFactory.Standard<OBJ, ATTR2> attribute) {
                return new ImmutableBsonConverterFactory2.Layer.WithoutId<>(this, attribute);
            }
        }
    }
}
