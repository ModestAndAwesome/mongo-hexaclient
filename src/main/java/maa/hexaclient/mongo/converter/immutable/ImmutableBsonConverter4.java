package maa.hexaclient.mongo.converter.immutable;

import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import maa.hexaclient.mongo.converter.immutable.attribute.ImmutableAttribute;
import org.bson.BsonDocument;

import static java.util.Objects.requireNonNull;

public record ImmutableBsonConverter4<OBJ, ATTR1, ATTR2, ATTR3, ATTR4>(
        ImmutableBsonConverter4.Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> parent,
        ImmutableBsonConverter4.Constructor<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> constructor
) implements ObjectBsonConverter<OBJ> {

    public ImmutableBsonConverter4(Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> parent, Constructor<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> constructor) {
        this.parent = requireNonNull(parent);
        this.constructor = requireNonNull(constructor);
    }

    @Override
    public BsonDocument toBson(OBJ object, ConverterContext context) {
        requireNonNull(object);
        requireNonNull(context);

        return parent.toBson(object, context);
    }

    @Override
    public OBJ toObject(BsonDocument bsonDocument, ConverterContext context) {
        return parent.toObject(bsonDocument, context, constructor);
    }

    @Override
    public Class<OBJ> convertedClass() {
        return parent.convertedClass();
    }

    public static final class Layer<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> {
        private final ImmutableBsonConverter3.Layer<OBJ, ATTR1, ATTR2, ATTR3> parent;
        private final ImmutableAttribute<OBJ, ATTR4> attribute;


        public Layer(ImmutableBsonConverter3.Layer<OBJ, ATTR1, ATTR2, ATTR3> parent, ImmutableAttribute<OBJ, ATTR4> attribute) {
            this.parent = requireNonNull(parent);
            this.attribute = requireNonNull(attribute);
        }


        public BsonDocument toBson(OBJ object, ConverterContext context) {
            return attribute.appendTo(parent.toBson(object, context), object, context);
        }

        public OBJ toObject(BsonDocument bsonDocument, ConverterContext context, Constructor<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> constructor) {
            return parent.toObject(bsonDocument,
                    context,
                    (attr1, attr2, attr3) ->
                            constructor.construct(attr1, attr2, attr3, attribute.retrieveValue(bsonDocument, context).orElse(null)));
        }

        public Class<OBJ> convertedClass() {
            return parent.convertedClass();
        }

        public ImmutableBsonConverter3.Layer<OBJ, ATTR1, ATTR2, ATTR3> parent() {
            return parent;
        }

        public ImmutableAttribute<OBJ, ATTR4> attribute() {
            return attribute;
        }
    }

    public interface Constructor<OBJ, ATTR1, ATTR2, ATTR3, ATTR4> {
        OBJ construct(ATTR1 attr1, ATTR2 attr2, ATTR3 attr3, ATTR4 attr4);
    }
}
