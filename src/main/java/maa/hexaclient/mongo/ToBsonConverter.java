package maa.hexaclient.mongo;

import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonValue;

public interface ToBsonConverter<OBJ> {

    BsonValue toBson(OBJ object, ConverterContext context);

}
