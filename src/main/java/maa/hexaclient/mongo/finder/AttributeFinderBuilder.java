package maa.hexaclient.mongo.finder;

import static java.util.Objects.requireNonNull;

public class AttributeFinderBuilder {

    private final String attributeName;

    public AttributeFinderBuilder(String attributeName) {
        this.attributeName = requireNonNull(attributeName);
    }

    public Finder eq(String value) {
        return new Finder(attributeName, value);
    }
}
