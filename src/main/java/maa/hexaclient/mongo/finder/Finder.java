package maa.hexaclient.mongo.finder;

import com.mongodb.client.model.Filters;
import org.bson.conversions.Bson;

public class Finder {

    private final String attributeName;
    private final String value;

    public Finder(String attributeName, String value) {
        this.attributeName = attributeName;
        this.value = value;
    }

    public static AttributeFinderBuilder path(String attributeName) {
        return new AttributeFinderBuilder(attributeName);
    }

    public Bson toBson() {
        return Filters.eq(attributeName, value);
    }
}
