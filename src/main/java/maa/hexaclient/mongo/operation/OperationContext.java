package maa.hexaclient.mongo.operation;

import com.mongodb.client.MongoCollection;
import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;
import org.bson.BsonDocument;
import org.bson.BsonValue;

import java.util.Objects;

import static java.util.Objects.hash;
import static java.util.Objects.requireNonNull;

public final class OperationContext<OBJ> {

    private final MongoCollection<BsonDocument> mongoCollection;
    private final ObjectBsonConverter<OBJ> converter;
    private final ConverterRegistry registry;
    private final ConverterContext converterContext = new ConverterContext();


    public OperationContext(
            MongoCollection<BsonDocument> mongoCollection,
            ObjectBsonConverter<OBJ> converter,
            ConverterRegistry registry
    ) {
        this.mongoCollection = requireNonNull(mongoCollection);
        this.converter = requireNonNull(converter);
        this.registry = requireNonNull(registry);
    }

    public OBJ convertRootToObject(BsonDocument document) {
        return converter.toObject(document, converterContext);
    }

    public BsonDocument convertRootToBson(OBJ object) {
        return converter.toBson(object, converterContext);
    }

    public BsonValue convertToBson(Object object) {
        return registry.mandatoryConvert(object, converterContext);
    }

    public MongoCollection<BsonDocument> mongoCollection() {
        return mongoCollection;
    }

    public ObjectBsonConverter<OBJ> converter() {
        return converter;
    }

    public ConverterRegistry registry() {
        return registry;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (OperationContext<?>) obj;
        return Objects.equals(this.mongoCollection, that.mongoCollection) &&
                Objects.equals(this.converter, that.converter) &&
                Objects.equals(this.registry, that.registry);
    }

    @Override
    public int hashCode() {
        return hash(mongoCollection, converter, registry);
    }

    @Override
    public String toString() {
        return "OperationContext[" +
                "mongoCollection=" + mongoCollection + ", " +
                "converter=" + converter + ", " +
                "registry=" + registry + ']';
    }
}
