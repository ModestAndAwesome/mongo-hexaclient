package maa.hexaclient.mongo.operation;

import org.bson.BsonDocument;

import java.util.Optional;

import static java.util.Objects.requireNonNull;

public class FindOneOperation<OBJ> {

    private final OperationContext<OBJ> operationContext;

    private final Object id;

    public FindOneOperation(OperationContext<OBJ> operationContext, Object id) {
        this.operationContext = operationContext;
        this.id = requireNonNull(id);
    }

    public Optional<OBJ> execute() {

        return Optional.ofNullable(operationContext.mongoCollection()
                        .find(new BsonDocument("_id", operationContext.convertToBson(id)))
                        .first())
                .map(operationContext::convertRootToObject);
    }

}
