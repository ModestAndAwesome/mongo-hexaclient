package maa.hexaclient.mongo.operation;

import maa.hexaclient.mongo.finder.Finder;

public class FindOperationBuilder<OBJ> {

    private final OperationContext<OBJ> operationContext;

    public FindOperationBuilder(OperationContext<OBJ> operationContext) {
        this.operationContext = operationContext;
    }

    public FindOneOperation<OBJ> byId(Object id) {
        return new FindOneOperation<>(operationContext, id);
    }

    public FindConditionalOperation<OBJ> where(Finder finder) {
        return new FindConditionalOperation<>(operationContext, finder);
    }

}
