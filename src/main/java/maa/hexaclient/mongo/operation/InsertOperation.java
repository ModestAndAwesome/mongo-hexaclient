package maa.hexaclient.mongo.operation;

import com.mongodb.client.MongoCollection;
import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import org.bson.BsonDocument;

import static java.util.Objects.requireNonNull;

public class InsertOperation<OBJ> {

    private final MongoCollection<BsonDocument> mongoCollection;
    private final ObjectBsonConverter<OBJ> converter;

    private final OBJ objectToInsert;

    public InsertOperation(MongoCollection<BsonDocument> mongoCollection,
                           ObjectBsonConverter<OBJ> converter,
                           OBJ objectToInsert) {

        this.mongoCollection = requireNonNull(mongoCollection);
        this.converter = requireNonNull(converter);
        this.objectToInsert = requireNonNull(objectToInsert);
    }

    public OBJ execute() {

        mongoCollection.insertOne(converter.toBson(objectToInsert, new ConverterContext()));

        return objectToInsert;
    }

}
