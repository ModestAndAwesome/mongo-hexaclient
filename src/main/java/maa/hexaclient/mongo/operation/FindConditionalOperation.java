package maa.hexaclient.mongo.operation;

import maa.hexaclient.mongo.finder.Finder;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.util.Objects.requireNonNull;

public class FindConditionalOperation<OBJ> {

    private final OperationContext<OBJ> context;
    private final Finder finder;

    public FindConditionalOperation(OperationContext<OBJ> context, Finder finder) {
        this.context = requireNonNull(context);
        this.finder = requireNonNull(finder);
    }

    public Stream<OBJ> execute() {
        return StreamSupport.stream(context.mongoCollection()
                        .find(finder.toBson())
                        .map(context::convertRootToObject)
                        .spliterator(),
                false);
    }
}
