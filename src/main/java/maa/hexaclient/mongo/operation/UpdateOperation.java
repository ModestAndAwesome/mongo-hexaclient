package maa.hexaclient.mongo.operation;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import org.bson.BsonDocument;

import static java.util.Objects.requireNonNull;

public class UpdateOperation<OBJ> {

    private final MongoCollection<BsonDocument> mongoCollection;
    private final ObjectBsonConverter<OBJ> converter;
    private final OBJ objectToUpdate;

    public UpdateOperation(MongoCollection<BsonDocument> mongoCollection,
                           ObjectBsonConverter<OBJ> converter,
                           OBJ objectToUpdate) {

        this.mongoCollection = requireNonNull(mongoCollection);
        this.converter = requireNonNull(converter);
        this.objectToUpdate = requireNonNull(objectToUpdate);
    }

    public OBJ execute() {

        final ConverterContext context = new ConverterContext();
        final BsonDocument document = converter.toBson(objectToUpdate, context);

        mongoCollection.updateOne(Filters.eq(document), document);

        return objectToUpdate;
    }

}
