package maa.hexaclient.mongo.query;

@FunctionalInterface
public interface QueryCriterionFactory<QUERY> {

    QueryCriterion<QUERY> create(QueryCriterionInitier<QUERY> initier);

}
