package maa.hexaclient.mongo.query;

import maa.hexaclient.mongo.converter.registry.ConverterRegistry;

import java.util.function.*;

public class QueryCriterionInitier<QUERY> {

    private final ConverterRegistry registry;

    public QueryCriterionInitier(ConverterRegistry registry) {
        this.registry = registry;
    }

    public <ATTR> QueryCriterionBuilder<QUERY, ATTR> from(Function<QUERY, ATTR> extractor) {

        return new QueryCriterionBuilder<>(registry, extractor);
    }
}
