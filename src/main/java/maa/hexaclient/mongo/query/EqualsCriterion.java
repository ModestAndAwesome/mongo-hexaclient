package maa.hexaclient.mongo.query;

import maa.hexaclient.mongo.converter.BsonConverter;
import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;
import org.bson.BsonDocument;

import java.util.function.*;

public class EqualsCriterion<QUERY, ATTR> implements QueryCriterion<QUERY> {

    private final Function<QUERY, ATTR> extractor;
    private final String attributeName;
    private final BsonConverter<ATTR> converter;

    public EqualsCriterion(Function<QUERY, ATTR> extractor, String attributeName, BsonConverter<ATTR> converter) {
        this.extractor = extractor;
        this.attributeName = attributeName;
        this.converter = converter;
    }

    @Override
    public BsonDocument appendTo(BsonDocument bsonDocument, QUERY queryObject, ConverterContext converterContext) {

        return bsonDocument.append(attributeName, converter.toBson(extractor.apply(queryObject), converterContext));
    }

    public static class Builder<QUERY, ATTR> {

        private final ConverterRegistry registry;
        private final Function<QUERY, ATTR> extractor;
        private final String attributeName;

        public Builder(ConverterRegistry registry, Function<QUERY, ATTR> extractor, String attributeName) {
            this.registry = registry;
            this.extractor = extractor;
            this.attributeName = attributeName;
        }

        public EqualsCriterion<QUERY, ATTR> as(Class<ATTR> attributeClass) {

            return new EqualsCriterion<>(extractor, attributeName, registry.getMandatoryConverterFor(attributeClass));
        }
    }
}
