package maa.hexaclient.mongo.query;

import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonDocument;

public interface QueryCriterion<QUERY> {

    BsonDocument appendTo(BsonDocument bsonDocument, QUERY query, ConverterContext converterContext);

}
