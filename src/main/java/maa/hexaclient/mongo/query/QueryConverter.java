package maa.hexaclient.mongo.query;

import maa.hexaclient.mongo.ToBsonConverter;
import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;
import org.bson.BsonDocument;
import org.bson.BsonValue;

import java.util.*;

import static java.util.Objects.*;

public class QueryConverter<QUERY> implements ToBsonConverter<QUERY> {

    private final Class<QUERY> queryClass;
    private final List<QueryCriterion<QUERY>> criteria;

    public QueryConverter(Class<QUERY> queryClass, List<QueryCriterion<QUERY>> criteria) {
        this.queryClass = requireNonNull(queryClass);
        this.criteria = Optional.ofNullable(criteria).map(List::copyOf).orElse(List.of());
    }

    public static <Q> QueryConverter.Builder<Q> forQuery(Class<Q> queryClass) {

        return new QueryConverter.Builder<>(queryClass);
    }

    @Override
    public BsonValue toBson(QUERY object, ConverterContext context) {

        final var result = new BsonDocument();
        criteria.forEach(criterion -> criterion.appendTo(result, object, context));
        return result;
    }

    public Class<QUERY> getQueryClass() {
        return queryClass;
    }

    public static class Builder<QUERY> {
        
        private final Class<QUERY> queryClass;
        private final List<QueryCriterionFactory<QUERY>> criterionFactories = new ArrayList<>();

        public Builder(Class<QUERY> queryClass) {
            this.queryClass = queryClass;
        }

        public Builder<QUERY> withCriteria(QueryCriterionFactory<QUERY> criteriaFactory) {

            this.criterionFactories.add(criteriaFactory);
            return this;
        }

        public QueryConverter<QUERY> create(ConverterRegistry registry) {

            final QueryCriterionInitier<QUERY> initier = new QueryCriterionInitier<>(registry);

            return new QueryConverter<>(queryClass, criterionFactories.stream().map(criterionFactory -> criterionFactory.create(initier)).toList());
        }
    }
    
    
    
}
