package maa.hexaclient.mongo.query;

import maa.hexaclient.mongo.converter.registry.ConverterRegistry;

import java.util.function.*;

public class QueryCriterionBuilder<QUERY, ATTR> {

    private final ConverterRegistry registry;
    private final Function<QUERY, ATTR> extractor;

    public QueryCriterionBuilder(ConverterRegistry registry, Function<QUERY, ATTR> extractor) {
        this.registry = registry;
        this.extractor = extractor;
    }

    public EqualsCriterion.Builder<QUERY, ATTR> toEquals(String attributeName) {

        return new EqualsCriterion.Builder<>(registry, extractor, attributeName);
    }
}
