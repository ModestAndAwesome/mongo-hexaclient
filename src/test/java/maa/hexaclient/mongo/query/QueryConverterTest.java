package maa.hexaclient.mongo.query;

import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;
import org.bson.BsonDocument;
import org.bson.BsonString;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class QueryConverterTest {

    private static final class DomainQuery {
        private final String name;

        public DomainQuery(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    @Test
    void should_create_a_query_when_configured_for_given_object() {

        // given
        final var query = new DomainQuery("Frank");
        final var converter = QueryConverter.forQuery(DomainQuery.class)
                .withCriteria(attr -> attr.from(DomainQuery::getName).toEquals("name").as(String.class))
                .create(ConverterRegistry.lang());

        // when
        final var bson = converter.toBson(query, new ConverterContext());

        // then
        assertThat(bson).isEqualTo(new BsonDocument("name", new BsonString("Frank")));
    }

}
