package maa.hexaclient.mongo.converter.immutable.attribute;

import maa.hexaclient.mongo.converter.ConversionException;
import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;
import org.bson.BsonDocument;
import org.bson.BsonNull;
import org.bson.BsonString;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

class ImmutableObjectAttributeTest {

    final ConverterRegistry registry = ConverterRegistry.lang();

    private record StringContainer(String attribute){}

    private final ConverterContext context = Mockito.mock(ConverterContext.class);

    @Test
    void should_append_valued_string_attribute() {

        // given
        final var attribute = ImmutableAttributeInitier.standard(registry)
                .from(StringContainer::attribute)
                .to("attribute")
                .as(String.class)
                .build();

        final var document = new BsonDocument();

        // when
        StringContainer container = new StringContainer("test");
        attribute.appendTo(document, container, context);

        // then
        assertThat(document).containsOnly(Map.entry("attribute", new BsonString("test")));
    }

    @Test
    void should_return_one_valued_string() {

        // given
        final var attribute = ImmutableAttributeInitier.standard(registry)
                .from(StringContainer::attribute)
                .to("attribute")
                .as(String.class)
                .build();

        final BsonDocument document = new BsonDocument("attribute", new BsonString("blah"));

        // when
        final Optional<String> value = attribute.retrieveValue(document, context);

        // then
        assertThat(value).contains("blah");
    }

    @Test
    void should_throw_exception_when_append_a_null_valued_mandatory_attribute() {

        // given
        final var attribute = ImmutableAttributeInitier.standard(registry)
                .from(StringContainer::attribute)
                .to("attribute")
                .as(String.class)
                .mandatory()
                .build();

        final var document = new BsonDocument();

        // when
        StringContainer container = new StringContainer(null);

        Assertions.assertThrows(ConversionException.class, () -> attribute.appendTo(document, container, context));
    }

    @Test
    void should_throw_exception_when_retrieve_a_null_valued_mandatory_attribute() {

        // given
        final var attribute = ImmutableAttributeInitier.standard(registry)
                .from(StringContainer::attribute)
                .to("attribute")
                .as(String.class)
                .mandatory()
                .build();

        final var document = new BsonDocument("attribute", BsonNull.VALUE);

        // when
        Assertions.assertThrows(ConversionException.class, () -> attribute.retrieveValue(document, context));
    }

    @Test
    void should_return_empty_optional_when_retrieving_null_valued_attribute() {

        // given
        final var attribute = ImmutableAttributeInitier.standard(registry)
                .from(StringContainer::attribute)
                .to("attribute")
                .as(String.class)
                .build();

        final BsonDocument document = new BsonDocument("attribute", BsonNull.VALUE);

        // when
        final Optional<String> value = attribute.retrieveValue(document, context);

        // then
        assertThat(value).isEmpty();
    }

    @Test
    void should_return_fallback_value() {

        // given
        final var attribute = ImmutableAttributeInitier.standard(registry)
                .from(StringContainer::attribute)
                .to("attribute")
                .as(String.class)
                .fallback("fallback")
                .build();

        final BsonDocument document = new BsonDocument("fallback", new BsonString("plop"));

        // when
        final Optional<String> value = attribute.retrieveValue(document, context);

        // then
        assertThat(value).contains("plop");
    }
}