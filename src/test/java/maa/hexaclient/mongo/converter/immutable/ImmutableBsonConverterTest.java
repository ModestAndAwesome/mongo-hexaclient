package maa.hexaclient.mongo.converter.immutable;


import maa.hexaclient.mongo.converter.BsonConverterFactory;
import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.ObjectBsonConverterFactory;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;
import org.bson.*;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class ImmutableBsonConverterTest {

    record TestObject(String id, SubObject object, long number) {}

    private final ObjectBsonConverterFactory<TestObject> testObjectConverterFactory = BsonConverterFactory
            .forImmutable(TestObject.class)
            .withId(id -> id.from(TestObject::id).as(String.class))
            .withAttribute(attr -> attr.from(TestObject::object).to("object").as(SubObject.class))
            .withAttribute(attr -> attr.from(TestObject::number).to("number").as(Long.class))
            .constructedBy(TestObject::new);


    record SubObject(String value) {}

    private final ObjectBsonConverterFactory<SubObject> subObjectCodec = BsonConverterFactory
            .forImmutable(SubObject.class)
            .withAttribute(attr -> attr.from(SubObject::value).to("value").as(String.class))
            .constructedBy(SubObject::new);


    private final ConverterRegistry registry = ConverterRegistry.composedOf(
            ConverterRegistry.lang(),
            ConverterRegistry.of(testObjectConverterFactory, subObjectCodec)
    ).cached();

    private final ConverterContext context = Mockito.mock(ConverterContext.class);

    @Test
    void should_create_sub_document_when_object_has_sub_object() {

        // given
        final BsonDocument document = new BsonDocument(List.of(
                new BsonElement("_id", new BsonString("myId")),
                new BsonElement("object", new BsonDocument("value", new BsonString("pouait"))),
                new BsonElement("number", new BsonInt64(123456L))
        ));

        // when
        final TestObject result = testObjectConverterFactory.create(registry).toObject(document, context);

        // then
        assertThat(result.id()).isEqualTo("myId");
        assertThat(result.object()).isEqualTo(new SubObject("pouait"));
        assertThat(result.number()).isEqualTo(123456L);
    }

	@Test
    void should_create_sub_object_when_document_has_sub_document() {

        // given
        final TestObject testObject = new TestObject(
                "myId",
                new SubObject("myValue"),
                123456L
        );

        // when
        final BsonValue result = testObjectConverterFactory.create(registry).toBson(testObject, context);

        // then
        assertThat(result).isInstanceOf(BsonDocument.class);
        final var document = (BsonDocument) result;
        assertThat(document.getString("_id")).isEqualTo(new BsonString("myId"));
        assertThat(document.getDocument("object")).isNotNull()
                .satisfies(subObject -> assertThat(subObject).containsOnly(Map.entry("value", new BsonString("myValue"))));
        assertThat(document.getInt64("number")).isEqualTo(new BsonInt64(123456L));
    }

    @Test
    void should_ignore_unmapped_attribute() {

        // given
        final BsonDocument document = new BsonDocument(List.of(
                new BsonElement("_id", new BsonString("myId")),
                new BsonElement("object", new BsonDocument("value", new BsonString("pouait"))),
                new BsonElement("number", new BsonInt64(1245L)),
                new BsonElement("notRecognizedAttribute", new BsonString("Can't catch me")),
                new BsonElement("unrecognizedNumber", new BsonInt32(42))
        ));

        // when
        final TestObject result = testObjectConverterFactory.create(registry).toObject(document, context);

        // then
        assertThat(result).isNotNull();
    }
}
