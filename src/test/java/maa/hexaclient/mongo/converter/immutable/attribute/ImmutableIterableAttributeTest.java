package maa.hexaclient.mongo.converter.immutable.attribute;

import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;
import org.bson.BsonArray;
import org.bson.BsonDocument;
import org.bson.BsonElement;
import org.bson.BsonString;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

class ImmutableIterableAttributeTest {

    private final ConverterRegistry registry = ConverterRegistry.lang();

    private record ListContainer(List<String> listAttribute){}

    private final ConverterContext context = Mockito.mock(ConverterContext.class);

    @Test
    void should_create_one_valued_list_attribute() {

        // given
        final var attribute = ImmutableAttributeInitier.standard(registry)
                .fromList(ListContainer::listAttribute)
                .to("listAttribute")
                .itemsAs(String.class)
                .build();

        // when
        ListContainer container = new ListContainer(List.of("test1", "test2"));
        final var document = new BsonDocument();

        attribute.appendTo(document, container, context);

        // then
        assertThat(document).containsExactly(Map.entry("listAttribute", new BsonArray(List.of(new BsonString("test1"), new BsonString("test2")))));
    }

    @Test
    void should_read_one_array() {

        // given
        final var attribute = ImmutableAttributeInitier.standard(registry)
                .fromList(ListContainer::listAttribute)
                .to("listAttribute")
                .itemsAs(String.class)
                .build();

        final BsonDocument document = new BsonDocument(List.of(
                new BsonElement("listAttribute", new BsonArray(List.of(
                        new BsonString("foo"),
                        new BsonString("bar"))))
        ));

        // when
        final Optional<List<String>> result = attribute.retrieveValue(document, context);

        // then
        assertThat(result).contains(List.of("foo", "bar"));
    }
}
