package maa.hexaclient.mongo.converter.immutable.attribute;

import maa.hexaclient.mongo.converter.BsonConverterFactory;
import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;
import org.bson.BsonDocument;
import org.bson.BsonInt64;
import org.bson.BsonString;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


class FlattenedImmutableAttributeTest {

    record SubContainer(String stringValue, long longValue) {}
    record Container(String id, SubContainer flattened) {}

    @Test
    void should_append_sub_attributes_to_root_document() {

        final ConverterRegistry registry = ConverterRegistry.lang();

        final FlattenedImmutableAttribute<Container, SubContainer> attribute = ImmutableAttributeInitier.standard(registry)
                .flattening(Container::flattened)
                .withConverter(BsonConverterFactory.forImmutable(SubContainer.class)
                        .withAttribute(attr -> attr.from(SubContainer::stringValue).to("stringValue").as(String.class))
                        .withAttribute(attr -> attr.from(SubContainer::longValue).to("longValue").as(Long.class))
                        .constructedBy(SubContainer::new))
                .build();

        final ConverterContext context = new ConverterContext();

        // when
        final Container container = new Container("blah", new SubContainer("string", 2L));
        final BsonDocument bsonDocument = new BsonDocument();

        final BsonDocument result = attribute.appendTo(bsonDocument, container, context);

        // then
        assertThat(result).containsOnly(
                Map.entry("stringValue", new BsonString("string")),
                Map.entry("longValue", new BsonInt64(2L))
        );
    }

    @Test
    void should_retrieve_sub_attributes_from_root_document() {

        final ConverterRegistry registry = ConverterRegistry.lang();

        final FlattenedImmutableAttribute<Container, SubContainer> attribute = ImmutableAttributeInitier.standard(registry)
                .flattening(Container::flattened)
                .withConverter(BsonConverterFactory.forImmutable(SubContainer.class)
                        .withAttribute(attr -> attr.from(SubContainer::stringValue).to("stringValue").as(String.class))
                        .withAttribute(attr -> attr.from(SubContainer::longValue).to("longValue").as(Long.class))
                        .constructedBy(SubContainer::new))
                .build();

        final ConverterContext context = new ConverterContext();

        // when
        final BsonDocument bsonDocument = new BsonDocument();
        bsonDocument.put("stringValue", new BsonString("Jo"));
        bsonDocument.put("longValue", new BsonInt64(2L));

        final Optional<SubContainer> result = attribute.retrieveValue(bsonDocument, context);

        // then
        assertThat(result).contains(new SubContainer("Jo", 2L));
    }

}