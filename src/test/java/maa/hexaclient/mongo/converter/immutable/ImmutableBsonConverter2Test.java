package maa.hexaclient.mongo.converter.immutable;

import maa.hexaclient.mongo.converter.BsonConverterFactory;
import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;
import org.assertj.core.api.InstanceOfAssertFactories;
import org.bson.*;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class ImmutableBsonConverter2Test {

    private final ConverterRegistry registry = ConverterRegistry.lang();

    private final ConverterContext context = Mockito.mock(ConverterContext.class);

    private record ObjectWith2Attributes(String name, int age) {
    }

    private final ObjectBsonConverter<ObjectWith2Attributes> bsonConverter = BsonConverterFactory
            .forImmutable(ObjectWith2Attributes.class)
            .withAttribute(attr -> attr.from(ObjectWith2Attributes::name).to("name").as(String.class))
            .withAttribute(attr -> attr.from(ObjectWith2Attributes::age).to("age").as(Integer.class))
            .constructedBy(ObjectWith2Attributes::new)
            .create(registry);


    @Test
    void should_create_object() {

        // given
        final var document = new BsonDocument(List.of(
                new BsonElement("name", new BsonString("gérard")),
                new BsonElement("age", new BsonInt32(43))
        ));

        // when
        final ObjectWith2Attributes result = bsonConverter.toObject(document, context);

        // then
        assertThat(result).isNotNull()
                .returns("gérard", ObjectWith2Attributes::name)
                .returns(43, ObjectWith2Attributes::age);
    }

    @Test
    void should_create_document() {

        // given
        final ObjectWith2Attributes testObject = new ObjectWith2Attributes("Gérard", 43);

        // when
        final var document = bsonConverter.toBson(testObject, context);

        // then
        assertThat(document).isInstanceOf(BsonDocument.class)
                .asInstanceOf(InstanceOfAssertFactories.map(String.class, BsonValue.class))
                .containsOnly(
                        Map.entry("name", new BsonString("Gérard")),
                        Map.entry("age", new BsonInt32(43))
                );
    }

    @Test
    void should_have_correct_converted_class() {

        assertThat(bsonConverter.convertedClass()).isEqualTo(ObjectWith2Attributes.class);
    }
}
