package maa.hexaclient.mongo.converter.immutable;

import maa.hexaclient.mongo.converter.BsonConverterFactory;
import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;
import org.assertj.core.api.InstanceOfAssertFactories;
import org.bson.BsonDocument;
import org.bson.BsonValue;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;

class ImmutableBsonConverter0Test {

    private final ConverterRegistry registry = ConverterRegistry.lang();

    private final ConverterContext context = Mockito.mock(ConverterContext.class);

    private record ObjectWith0Attribute() {}

    private final ObjectBsonConverter<ObjectWith0Attribute> bsonConverter = BsonConverterFactory
            .forImmutable(ObjectWith0Attribute.class)
            .constructedBy(ObjectWith0Attribute::new)
            .create(registry);


    @Test
    void should_create_object() {

        // given
        final var document = new BsonDocument();

        // when
        final ObjectWith0Attribute result = bsonConverter.toObject(document, context);

        // then
        assertThat(result).isNotNull();
    }

    @Test
    void should_create_document() {

        // given
        final ObjectWith0Attribute testObject = new ObjectWith0Attribute();

        // when
        final var document = bsonConverter.toBson(testObject, context);

        // then
        assertThat(document)
                .isInstanceOf(BsonDocument.class)
                .asInstanceOf(InstanceOfAssertFactories.map(String.class, BsonValue.class))
                .isEmpty();
    }

    @Test
    void should_have_correct_converted_class() {

        assertThat(bsonConverter.convertedClass()).isEqualTo(ObjectWith0Attribute.class);
    }
}
