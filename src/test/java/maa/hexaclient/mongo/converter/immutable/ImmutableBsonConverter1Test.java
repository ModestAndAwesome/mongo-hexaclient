package maa.hexaclient.mongo.converter.immutable;

import maa.hexaclient.mongo.converter.BsonConverterFactory;
import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;
import org.assertj.core.api.InstanceOfAssertFactories;
import org.bson.BsonDocument;
import org.bson.BsonElement;
import org.bson.BsonString;
import org.bson.BsonValue;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class ImmutableBsonConverter1Test {

    private final ConverterRegistry registry = ConverterRegistry.lang();

    private final ConverterContext context = Mockito.mock(ConverterContext.class);

    private record ObjectWith1Attribute(String name) {
    }

    private final ObjectBsonConverter<ObjectWith1Attribute> bsonConverter = BsonConverterFactory
            .forImmutable(ObjectWith1Attribute.class)
            .withAttribute(attr -> attr.from(ObjectWith1Attribute::name).to("name").as(String.class))
            .constructedBy(ObjectWith1Attribute::new)
            .create(registry);


    @Test
    void should_create_object() {

        // given
        final var document = new BsonDocument(List.of(
                new BsonElement("name", new BsonString("gérard"))
        ));

        // when
        final ObjectWith1Attribute result = bsonConverter.toObject(document, context);

        // then
        assertThat(result).isNotNull()
                .returns("gérard", ObjectWith1Attribute::name);
    }

    @Test
    void should_create_document() {

        // given
        final ObjectWith1Attribute testObject = new ObjectWith1Attribute("Gérard");

        // when
        final var document = bsonConverter.toBson(testObject, context);

        // then
        assertThat(document).isInstanceOf(BsonDocument.class)
                .asInstanceOf(InstanceOfAssertFactories.map(String.class, BsonValue.class))
                .containsOnly(
                        Map.entry("name", new BsonString("Gérard"))
                );
    }

    @Test
    void should_have_correct_converted_class() {

        assertThat(bsonConverter.convertedClass()).isEqualTo(ObjectWith1Attribute.class);
    }
}
