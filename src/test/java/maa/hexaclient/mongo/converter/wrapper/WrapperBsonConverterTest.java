package maa.hexaclient.mongo.converter.wrapper;

import maa.hexaclient.mongo.converter.BsonConverterFactory;
import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.ObjectBsonConverterFactory;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;
import org.assertj.core.api.InstanceOfAssertFactories;
import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.BsonValue;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;

class WrapperBsonConverterTest {

    private record TestObject(String value) {}
    private record Container(TestObject testObject) {}

    private final BsonConverterFactory<TestObject> testObjectConverterFactory =
            BsonConverterFactory.forWrapper(TestObject.class)
                    .delegate(TestObject::value, TestObject::new)
                    .as(String.class);

    private final ObjectBsonConverterFactory<Container> containerConverterFactory =
            BsonConverterFactory.forImmutable(Container.class)
                    .withAttribute(attr -> attr.from(Container::testObject).to("testObject").as(TestObject.class))
                    .constructedBy(Container::new);

    private final ConverterRegistry registry = ConverterRegistry.composedOf(
            ConverterRegistry.lang(),
            ConverterRegistry.of(testObjectConverterFactory, containerConverterFactory)
    );

    private final ConverterContext context = Mockito.mock(ConverterContext.class);

    @Test
    void should_create_object() {
        // given
        final BsonValue bson = new BsonString("test");

        // when
        final TestObject result = testObjectConverterFactory.create(registry).toObject(bson, context);

        // then
        assertThat(result.value()).isEqualTo("test");
    }

    @Test
    void should_create_object_containing_a_string() {
        // given
        final BsonDocument bsonDocument = new BsonDocument("testObject", new BsonString("myValue"));

        // when
        final Container result = containerConverterFactory.create(registry).toObject(bsonDocument, context);

        // then
        assertThat(result.testObject).isEqualTo(new TestObject("myValue"));
    }

    @Test
    void should_create_bson_string() {
        // given
        final Container container = new Container(new TestObject("myValue"));

        // when
        final BsonValue bsonDocument = containerConverterFactory.create(registry).toBson(container, context);

        // then
        assertThat(bsonDocument).isInstanceOf(BsonDocument.class)
                .asInstanceOf(InstanceOfAssertFactories.map(String.class, BsonValue.class))
                .containsEntry("testObject", new BsonString("myValue"));
    }


}