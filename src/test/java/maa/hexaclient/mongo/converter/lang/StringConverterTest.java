package maa.hexaclient.mongo.converter.lang;

import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonString;
import org.bson.BsonValue;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;


class StringConverterTest {

    @Test
    void should_create_bson_double() {

        // given
        final StringConverter converter = new StringConverter();
        final ConverterContext context = Mockito.mock(ConverterContext.class);

        // when
        final BsonValue result = converter.toBson("Test", context);

        // then
        assertThat(result).isEqualTo(new BsonString("Test"));
    }

    @Test
    void should_create_double() {

        // given
        final StringConverter converter = new StringConverter();
        final ConverterContext context = Mockito.mock(ConverterContext.class);

        // when
        final String result = converter.toObject(new BsonString("Hop"), context);

        // then
        assertThat(result).isEqualTo("Hop");
    }


}