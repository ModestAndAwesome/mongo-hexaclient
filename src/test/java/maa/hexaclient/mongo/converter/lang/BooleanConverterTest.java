package maa.hexaclient.mongo.converter.lang;


import maa.hexaclient.mongo.converter.ConverterContext;
import org.assertj.core.api.Assertions;
import org.bson.BsonBoolean;
import org.bson.BsonValue;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class BooleanConverterTest {

    @Test
    void should_create_bson_boolean() {
        // given
        final ConverterContext context = Mockito.mock(ConverterContext.class);
        final BooleanConverter converter = new BooleanConverter();

        // when
        final BsonValue value = converter.toBson(true, context);

        // then
        Assertions.assertThat(value).isEqualTo(new BsonBoolean(true));
    }

    @Test
    void should_create_boolean() {
        // given
        final ConverterContext context = Mockito.mock(ConverterContext.class);
        final BooleanConverter converter = new BooleanConverter();

        // when
        final Boolean result = converter.toObject(new BsonBoolean(true), context);

        // then
        Assertions.assertThat(result).isTrue();
    }
}