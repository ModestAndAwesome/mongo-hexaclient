package maa.hexaclient.mongo.converter.lang;

import maa.hexaclient.mongo.converter.ConverterContext;
import org.bson.BsonBinary;
import org.bson.BsonValue;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;

class ByteArrayConverterTest {

    @Test
    void should_create_bson_binary() {
        //given
        final ByteArrayConverter converter = new ByteArrayConverter();
        ConverterContext context = Mockito.mock(ConverterContext.class);

        // when
        final BsonValue value = converter.toBson(new byte[]{1, 2}, context);

        // then
        assertThat(value).isEqualTo(new BsonBinary(new byte[]{1, 2}));
    }

    @Test
    void should_create_byte_array() {
        //given
        final ByteArrayConverter converter = new ByteArrayConverter();
        ConverterContext context = Mockito.mock(ConverterContext.class);

        // when
        final byte[] object = converter.toObject(new BsonBinary(new byte[]{1, 2}), context);

        // then
        assertThat(object).isEqualTo(new byte[]{1, 2});
    }

}