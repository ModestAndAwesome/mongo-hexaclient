package maa.hexaclient.mongo.converter.lang;

import maa.hexaclient.mongo.converter.ConverterContext;
import org.assertj.core.api.Assertions;
import org.bson.BsonInt64;
import org.bson.BsonValue;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;


class LongConverterTest {

    @Test
    void should_create_bson_double() {

        // given
        final LongConverter converter = new LongConverter();
        final ConverterContext context = Mockito.mock(ConverterContext.class);

        // when
        final BsonValue result = converter.toBson(456L, context);

        // then
        Assertions.assertThat(result).isEqualTo(new BsonInt64(456L));
    }

    @Test
    void should_create_double() {

        // given
        final LongConverter converter = new LongConverter();
        final ConverterContext context = Mockito.mock(ConverterContext.class);

        // when
        final Long result = converter.toObject(new BsonInt64(654L), context);

        // then
        Assertions.assertThat(result).isEqualTo(654L);
    }

}