package maa.hexaclient.mongo.converter.lang;

import maa.hexaclient.mongo.converter.ConverterContext;
import org.assertj.core.api.Assertions;
import org.bson.BsonInt32;
import org.bson.BsonValue;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;


class IntegerConverterTest {

    @Test
    void should_create_bson_double() {

        // given
        final IntegerConverter converter = new IntegerConverter();
        final ConverterContext context = Mockito.mock(ConverterContext.class);

        // when
        final BsonValue result = converter.toBson(28_742, context);

        // then
        Assertions.assertThat(result).isEqualTo(new BsonInt32(28_742));
    }

    @Test
    void should_create_double() {

        // given
        final IntegerConverter converter = new IntegerConverter();
        final ConverterContext context = Mockito.mock(ConverterContext.class);

        // when
        final Integer result = converter.toObject(new BsonInt32(28_742), context);

        // then
        Assertions.assertThat(result).isEqualTo(28_742);
    }
}