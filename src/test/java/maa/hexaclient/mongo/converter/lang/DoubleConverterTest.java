package maa.hexaclient.mongo.converter.lang;

import maa.hexaclient.mongo.converter.ConverterContext;
import org.assertj.core.api.Assertions;
import org.bson.BsonDouble;
import org.bson.BsonValue;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class DoubleConverterTest {

    @Test
    void should_create_bson_double() {

        // given
        final DoubleConverter converter = new DoubleConverter();
        final ConverterContext context = Mockito.mock(ConverterContext.class);

        // when
        final BsonValue result = converter.toBson(3d, context);

        // then
        Assertions.assertThat(result).isEqualTo(new BsonDouble(3d));
    }

    @Test
    void should_create_double() {

        // given
        final DoubleConverter converter = new DoubleConverter();
        final ConverterContext context = Mockito.mock(ConverterContext.class);

        // when
        final Double result = converter.toObject(new BsonDouble(3d), context);

        // then
        Assertions.assertThat(result).isEqualTo(3d);
    }
}
