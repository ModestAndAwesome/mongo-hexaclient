package maa.hexaclient.mongo.converter.lang;

import maa.hexaclient.mongo.converter.ConverterContext;
import org.assertj.core.api.Assertions;
import org.bson.BsonDouble;
import org.bson.BsonValue;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;


class FloatConverterTest {

    @Test
    void should_create_bson_double() {

        // given
        final FloatConverter converter = new FloatConverter();
        final ConverterContext context = Mockito.mock(ConverterContext.class);

        // when
        final BsonValue result = converter.toBson(3f, context);

        // then
        Assertions.assertThat(result).isEqualTo(new BsonDouble(3d));
    }

    @Test
    void should_create_float() {

        // given
        final FloatConverter converter = new FloatConverter();
        final ConverterContext context = Mockito.mock(ConverterContext.class);

        // when
        final Float result = converter.toObject(new BsonDouble(3d), context);

        // then
        Assertions.assertThat(result).isEqualTo(3f);
    }
}