package maa.hexaclient.mongo.converter.dispatch;

import maa.hexaclient.mongo.converter.BsonConverterFactory;
import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.ObjectBsonConverterFactory;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;
import org.assertj.core.api.InstanceOfAssertFactories;
import org.bson.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class DispatchBsonConverterTest {

    interface Vehicle {}
    record Car(int numberOfWheel) implements Vehicle {}
    record Plane(String companyName) implements Vehicle {}
    record Boat(long CaptainAge) implements Vehicle {}

    private final ConverterRegistry registry = ConverterRegistry.composedOf(
            ConverterRegistry.lang(),
            ConverterRegistry.of(
                    BsonConverterFactory.forImmutable(Car.class)
                            .withAttribute(attr -> attr.from(Car::numberOfWheel).to("numberOfWheel").as(Integer.class))
                            .constructedBy(Car::new)
            )
    ).cached();

    private final ObjectBsonConverterFactory<Vehicle> vehicleBsonConverterFactory =
            BsonConverterFactory.dispatch(Vehicle.class)
                    .on("type")
                    .dispatch(dispatch -> dispatch.alias("car").as(Car.class))
                    .dispatch(dispatch -> dispatch.alias("plane").using(BsonConverterFactory.forImmutable(Plane.class)
                            .withAttribute(attr -> attr.from(Plane::companyName).to("companyName").as(String.class))
                            .constructedBy(Plane::new)))
                    .otherwise()
                    .returnNull();

    private final ConverterContext context = Mockito.mock(ConverterContext.class);

    @ParameterizedTest
    @MethodSource("createObjects")
    void should_convert_given_document_to_expected_object(BsonDocument document, Vehicle expected) {

        // when
        final Vehicle result = vehicleBsonConverterFactory.create(registry).toObject(document, context);

        // then
        assertThat(result).isEqualTo(expected);
    }

    static Stream<Arguments> createObjects() {

        return Stream.<Arguments>builder()
                .add(Arguments.of(
                        new BsonDocument(List.of(
                                new BsonElement("type", new BsonString("car")),
                                new BsonElement("numberOfWheel", new BsonInt32(4))
                        )),
                        new Car(4)
                ))
                .add(Arguments.of(
                        new BsonDocument(List.of(
                                new BsonElement("type", new BsonString("plane")),
                                new BsonElement("companyName", new BsonString("Airbus"))
                        )),
                        new Plane("Airbus")
                ))
                .add(Arguments.of(
                        new BsonDocument(List.of(
                                new BsonElement("type", new BsonString("unknownVehicle")),
                                new BsonElement("sound", new BsonString("loud"))
                        )),
                        null
                )).build();
    }

    @Test
    void should_write_plane_on_encoding_plane_object() {
        // given
        final Plane plane = new Plane("Airbus");

        // when
        var document = vehicleBsonConverterFactory.create(registry).toBson(plane, context);

        // then
        assertThat(document).isInstanceOf(BsonDocument.class)
                .asInstanceOf(InstanceOfAssertFactories.map(String.class, BsonValue.class))
                .containsOnly(
                        Map.entry("type", new BsonString("plane")),
                        Map.entry("companyName", new BsonString("Airbus"))
                );
    }

    @Test
    void should_write_car_on_encoding_car_object() {

        // given
        final Car car = new Car(4);

        // when
        var document = vehicleBsonConverterFactory.create(registry).toBson(car, context);

        // then
        assertThat(document).isInstanceOf(BsonDocument.class)
                .asInstanceOf(InstanceOfAssertFactories.map(String.class, BsonValue.class))
                .containsOnly(
                        Map.entry("type", new BsonString("car")),
                        Map.entry("numberOfWheel", new BsonInt32(4))
                );
    }

    @Test
    void should_write_null_on_encoding_boat_object() {

        // given
        final Boat boat = new Boat(52L);

        // when
        var document = vehicleBsonConverterFactory.create(registry).toBson(boat, context);

        // then
        assertThat(document).isInstanceOf(BsonDocument.class)
                .asInstanceOf(InstanceOfAssertFactories.map(String.class, BsonValue.class))
                .isEmpty();
    }
}
