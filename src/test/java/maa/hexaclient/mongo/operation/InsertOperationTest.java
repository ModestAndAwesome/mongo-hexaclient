package maa.hexaclient.mongo.operation;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.InsertOneResult;
import maa.hexaclient.mongo.converter.BsonConverterFactory;
import maa.hexaclient.mongo.converter.ConverterContext;
import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;
import org.bson.BsonDocument;
import org.bson.BsonString;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;


class InsertOperationTest {

    private record TestObject(String id, int number){}

    @Test
    void should_call_insert_on_mongo_collection() {

        // given
        final TestObject testObject = new TestObject("myId", 3);

        final ObjectBsonConverter<TestObject> converter = BsonConverterFactory.forImmutable(TestObject.class)
                .withId(id -> id.from(TestObject::id).as(String.class))
                .withAttribute(attr -> attr.from(TestObject::number).to("number").as(Integer.class))
                .constructedBy(TestObject::new)
                .create(ConverterRegistry.lang());


        //noinspection unchecked
        final MongoCollection<BsonDocument> mongoCollection = Mockito.mock(MongoCollection.class);
        BDDMockito.given(mongoCollection.insertOne(converter.toBson(testObject, new ConverterContext())))
                .willReturn(InsertOneResult.acknowledged(new BsonString("myId")));

        final InsertOperation<TestObject> insertOperation = new InsertOperation<>(mongoCollection, converter, testObject);

        // when
        final TestObject result = insertOperation.execute();

        // then
        assertThat(result).isEqualTo(testObject);
    }


}