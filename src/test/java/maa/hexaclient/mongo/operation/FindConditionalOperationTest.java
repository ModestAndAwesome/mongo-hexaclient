package maa.hexaclient.mongo.operation;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import maa.hexaclient.mongo.EmbeddedMongoExtension;
import maa.hexaclient.mongo.converter.BsonConverterFactory;
import maa.hexaclient.mongo.converter.ObjectBsonConverter;
import maa.hexaclient.mongo.converter.registry.ConverterRegistry;
import maa.hexaclient.mongo.finder.Finder;
import org.assertj.core.api.Assertions;
import org.bson.BsonDocument;
import org.bson.BsonElement;
import org.bson.BsonString;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.List;
import java.util.stream.Collectors;

@ExtendWith(EmbeddedMongoExtension.class)
class FindConditionalOperationTest {

    private record MyObject(String id, String name){}

    private final ConverterRegistry registry = ConverterRegistry.lang();

    private final ObjectBsonConverter<MyObject> objectConverter = BsonConverterFactory.forImmutable(MyObject.class)
            .withId(id -> id.from(MyObject::id).as(String.class))
            .withAttribute(attr -> attr.from(MyObject::name).to("name").as(String.class))
            .constructedBy(MyObject::new)
            .create(registry);

    @Test
    @Tag("integration")
    void should_find_one_result(MongoClient mongoClient) {

        // given
        final MongoCollection<BsonDocument> mongoCollection = mongoClient.getDatabase("test")
                .getCollection("test", BsonDocument.class);
        mongoCollection.insertOne(new BsonDocument(List.of(
                new BsonElement("_id", new BsonString("myId")),
                new BsonElement("name", new BsonString("value"))
        )));
        mongoCollection.insertOne(new BsonDocument(List.of(
                new BsonElement("_id", new BsonString("anotherId")),
                new BsonElement("name", new BsonString("anotherValue"))
        )));

        final Finder finder = Finder.path("name").eq("value");

        final OperationContext<MyObject> operationContext = new OperationContext<>(mongoCollection, objectConverter, registry);
        final FindConditionalOperation<MyObject> operation = new FindConditionalOperation<>(operationContext, finder);

        // when
        final var result = operation.execute().collect(Collectors.toSet());

        // then
        Assertions.assertThat(result)
                .containsOnly(new MyObject("myId", "value"));
    }
}